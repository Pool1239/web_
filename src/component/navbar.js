import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Nav, NavItem, NavDropdown, Navbar, MenuItem, Button } from 'react-bootstrap';
import './navbar.css';
import PS from '../img/office one logo.png';
import Modal from "react-responsive-modal";
import FileBase64 from 'react-file-base64';
import { get, post, sever } from '../service/api';
import swal from 'sweetalert';

class navbar extends Component {
  constructor(props) {
    super(props)

    this.state = {
      userdata: [],
      showdata: [],
      open: false,
      files: []
    }
  }
  onOpenModal = async () => {
    this.setState({ open: true })
  }

  onCloseModal = () => {
    this.setState({ open: false })
    window.location.reload()
  }

  componentDidMount = async () => {

    let userdata = JSON.parse(localStorage.getItem("userdata"))
    // this.setState({userdata})
    // console.log('nobar', userdata)
    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    // try {
    //   const res = await post('/show_username_my_leave_request',{empID:userdata})
    //   this.setState({ showdata : res.result })
    // } catch (error) {
    //   alert(error)
    // }
    try {

    } catch (error) {
      swal("Error!", "Error", "error", {
        buttons: false,
        timer: 1200,
      })
    }
  }

  getFiles(files) {
    files.map(el => {
      if (el.type === "image/png" || el.type === "image/jpg") {
        this.setState({ files: files, imagePreviewUrl: files[0].base64 })
        // console.log(files[0].base64)
      }
      else {
        swal("Warning!", "Please select images", "error", {
          buttons: false,
          timer: 1200,
        });
      }
    })
  }

  getBase64 = ({ target: { files } }) => {

    var reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onload = () => {
      console.log(reader.result);
      // alert(reader.result)
      this.setState({ files: reader.result, imagePreviewUrl: reader.result })
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
  }

  // componentDidMount = () => {
  //   let userdata = localStorage.getItem("userdata")
  //   if(userdata) {
  //     this.setState({userdata})
  //   } else {
  //     this.setState({userdata: []})
  //   }
  //   console.log('nobar', userdata)

  // }
  _onClick = async () => {

    let userdata = JSON.parse(localStorage.getItem("userdata"))

    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    const { files } = this.state

    const empID = parseInt(userdata[0].empID)
    const obj = {
      empID, image: files
    }
    console.log(obj)
    try {
      await post('/insert_profile', obj)
      window.location.reload()
    } catch (error) {
      swal("Error!", "Error", "error", {
        buttons: false,
        timer: 1200,
      })
    }
  }

  logout = async () => {
    try {
      await get('/logout')
      this.props.history.push('/')
      window.location.reload()
    } catch (error) {
      swal("Error!", "Error", "error", {
        buttons: false,
        timer: 1200,
      })
    }
  }
  render() {
    const { open } = this.state;
    const { pathname } = this.props.location;
    let { imagePreviewUrl } = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<img className='imgUpload' src={imagePreviewUrl} />);
    } else {
      $imagePreview = (<div style={{ border: '2px dashed #05cdff', width: 200, height: 200, marginBottom: 20 }}></div>);
    }
    return (
      <div>
        <Modal open={open} onClose={this.onCloseModal} center className='ModalApprove' >
          <div className='headerModal'>
            <p style={{ fontSize: 24, marginTop: 10, textAlign: 'center', fontWeight: 500 }}>Change Profile Picture</p>
          </div>
          <div className='solidmodal123' />
          <div style={{ textAlign: 'center' }}>
            <div style={{ display: 'flex', justifyContent: 'center', flexDirection: 'column', alignItems: 'center' }}>
              <div>{$imagePreview}</div>
              {/* <p className='showww'> */}
              {/* <FileBase64 width={'100px'} multiple={true} onDone={this.getFiles.bind(this)} /> */}
              {imagePreviewUrl ?
                <div>
                  <input type="file" name="photo" id="photo" class="changeImgProfileED" accept="image/*" onChange={this.getBase64}
                    onClick={(event) => {
                      event.target.value = null
                    }} />
                  <label for="photo">Choose a picture</label>
                </div>
                :
                <div>
                  <input type="file" name="photo" id="photo" class="changeImgProfile" accept="image/*" onChange={this.getBase64}
                    onClick={(event) => {
                      event.target.value = null
                    }} />
                  <label for="photo">Choose a picture</label>
                </div>
              }

              {/* </p> */}
            </div>
            {
              imagePreviewUrl ?
                <div >
                  <input type="button" value="Enter" className="submitProfileED" onClick={this._onClick} />
                  <input type="button" value="Cancel" className="cancelProfile" onClick={this.onCloseModal} />
                </div>
                :
                <div >
                  <input type="button" value="Enter" className="submitProfile" onClick={this._onClick} />
                  <input type="button" value="Cancel" className="cancelProfileED" onClick={this.onCloseModal} />
                </div>
            }

          </div>
        </Modal>

        <Navbar inverse collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <img src={PS} style={{ height: 50 }} />
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>
              <NavItem href='/AllLeaveRequest'>
                Leave Request
              </NavItem>
              <NavItem href='/AllFlexibleRequest'>
                Flexible Benefit
              </NavItem>
              <NavItem href='/AllUsers'>
                User
              </NavItem>
              <NavDropdown eventKey={3} title="Report" id="basic-nav-dropdown">
                <MenuItem eventKey={3.1}><Link to='/LeaveReport'>Leave Report</Link></MenuItem>
                <MenuItem divider />
                <MenuItem eventKey={3.2}><Link to="/FlexBenefitReport">Flexible Benefit Report</Link></MenuItem>
              </NavDropdown>
              <NavItem href='/leave_request'>
                My Leave Request
              </NavItem>
              <NavItem href='/AllCompany' >
                Company
              </NavItem>
              <NavItem href='/AllProject' >
                Project
              </NavItem>
            </Nav>
            <Nav pullRight>
              <Nav onClick={this.onOpenModal}>
                <div className='navdro2'>
                  <img className='navdro' src={sever + '/user_profiles'} onClick={this.onOpenModal} />
                </div>
              </Nav>
              <NavItem onClick={this.logout}>
                Logout
      </NavItem>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div >
    )
  }
}
export default withRouter(navbar)
