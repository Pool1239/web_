import React, { Component } from 'react'
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import login from '../pages/login'
import AppRoute from './app'
import confirm from '../pages/confirm'
import LeaveRequest from '../pages/Page_LeaveRequest'
import employee from '../pages/employee'
import flexible from '../pages/Flexible'
import checkworking from '../pages/checkworking'

export default () => (
    <BrowserRouter>
        <Switch>
            <Route exact path='/' component={login} />
            <Route path='/confirm' component={confirm} />
            <Route path='/LeaveRequest' component={LeaveRequest} />
            <Route path='/employee' component={employee} />
            <Route path='/flexible' component={flexible} />
            <Route path='/checkworking' component={checkworking} />
            <AppRoute></AppRoute>
        </Switch>
    </BrowserRouter>
)
