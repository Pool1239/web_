import React, { Component } from 'react'
import { Route } from 'react-router-dom'

import App from '../App'
import AllUsers from '../pages/Page_User'
import AllLeaveRequest from '../pages/Page_AllLeaveRequest'
import report from '../pages/report'
import LeaveReport from '../pages/Page_LeaveReport'
import FlexBenefitReport from '../pages/Page_BenefitReport'
import leave_request from '../pages/leave request'
import create from '../pages/create user'
import AllFlexibleRequest from '../pages/Page_AllFlexibleRequest';
import AllCompany from '../pages/Page_AllCompany'
import create_admin_user from '../pages/create_admin_and_user'
import update_leave_type from '../pages/update_leave_type'
import AllProject from '../pages/Page_AllProject'
import AllPeople from '../pages/Page_AllPeople'
import job from '../pages/job'
import job2 from '../pages/job2'
// import checkworking from '../pages/checkworking'

export default () => <App>
    <Route path='/AllUsers' component={AllUsers} />
    <Route path='/AllLeaveRequest' component={AllLeaveRequest} />
    <Route path='/report' component={report} />
    <Route path='/LeaveReport' component={LeaveReport} />
    <Route path='/FlexBenefitReport' component={FlexBenefitReport} />
    <Route path='/leave_request' component={leave_request} />
    <Route path='/create' component={create} />
    <Route path='/AllFlexibleRequest' component={AllFlexibleRequest} />
    <Route path='/AllCompany' component={AllCompany} />
    <Route path='/create_admin_user' component={create_admin_user} />
    <Route path='/update_leave_type' component={update_leave_type} />
    <Route path='/AllProject' component={AllProject} />
    <Route path='/AllPeople' component={AllPeople} />
    <Route path='/job' component={job} />
    <Route path='/job2' component={job2} />
    {/* <Route path='/checkworking' component={checkworking}/> */}
</App>
