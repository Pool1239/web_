import React, { Component } from 'react'
import '../pages/Leave.css'
import { get, post, sever } from '../service/api'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import moment from 'moment';
import searc from '../img/magnifying-glass.png'
import Modal from "react-responsive-modal"
import './company.css'
import add from '../img/magnifying-glass.png'
import { Link } from 'react-router-dom';
import swit from '../img/switch (2).png';
import swal from 'sweetalert';
import { FormControl, FormGroup, ControlLabel } from 'react-bootstrap';

export default class AllProject extends Component {
  constructor(props) {
    super(props)

    this.state = {
      comID: '', project_name: '', project_start: '', project_end: '', user_create: '', project_id: '',
      time_in: '',
      time_out: '',
      comnames: '',
      userName: '',
      emp_name: '',
      emp_last: '',
      email: '',
      pro_period: '1',
      role: '1',
      comname: '',
      dataTables: [],
      showdata: [],
      result: {
        userName: ''
      }
    }
  }
  state = {
    open2: false,
    addProject: false,
  };
  onOpenModal2 = async (cell) => {
    try {
      const res = await post('/show_project2', { project_id: cell })
      if (res.success) {
        this.setState({
          open2: true, project_name: res.result[0].project_name,
          project_start: res.result[0].project_start, project_end: res.result[0].project_end,
          project_id: res.result[0].project_id
        })
      } else {
        swal("Error!", res.message, "error", {
          buttons: false,
          timer: 1200,
        })
      }

    } catch (error) {
      swal("Error!", "Error", "error", {
        buttons: false,
        timer: 1200,
      })
    }
  }
  onCloseModal2 = () => {
    this.setState({ open2: false })
  }
  onCloseAddProject = () => {
    this.setState({ addProject: false, project_name: '', project_start: '', project_end: '' })
  }
  handleAddProject = (e) => {
    this.setState({ project_name: e.target.value });
  }
  handleAddProjectStart = (e) => {
    this.setState({ project_start: e.target.value });
  }

  handleAddProjectEnd = (e) => {
    this.setState({ project_end: e.target.value });
  }
  _onChange = e => {
    this.setState({ [e.target.name]: e.target.value })

    // console.log(this.state.userName)
  }
  getValidationAddProjectNameState() {
    const project = this.state.project_name;
    const length = this.state.project_name.length;
    if (length >= 1 && project != ' ') return 'success';
    else if (length >= 1 && project === ' ') return 'warning';
    else if (length === 0) return 'error';
    return null;
  }
  getValidationProjectStart_State() {
    const project_start = this.state.project_start;
    const length = this.state.project_start.length;
    if (length >= 1 && project_start != ' ') return 'success';
    else if (length >= 1 && project_start === ' ') return 'warning';
    else if (length === 0) return 'error';
    return null;
  }

  getValidationProjectEnd_State() {
    const project_end = this.state.project_end;
    const length = this.state.project_end.length;
    if (length >= 1 && project_end != ' ') return 'success';
    else if (length >= 1 && project_end === ' ') return 'warning';
    else if (length === 0) return 'error';
    return null;
  }
  componentWillMount = () => {
    let res = localStorage.getItem('userdata');
    if (res) {
      this.setState({ result: JSON.parse(res)[0] })
    }
  }
  componentDidMount = async () => {
    if (this.state.result.userName) {
      const q = await get('/show_leave_request')
      if (q.success) {
        if (this.state.result.company == 1) {
          const res = await get('/project')
          if (res.success) {
            this.setState({ dataTables: res.result })
          } else {
            this.props.history.push('/page2')
            window.location.reload()
          }
        }
        else {
          const res = await post('/show_project_by_company', { company: this.state.result.company })
          if (res.success) {
            this.setState({ dataTables: res.result })
          } else {
            this.props.history.push('/page2')
            window.location.reload()
          }
        }
      } else {
        this.props.history.push('/')
        window.location.reload()
      }
    }
    else {
      this.props.history.push('/')
      window.location.reload()
    }
  }
  _onClick = async () => {
    if (this.state.project_name == "") {
      swal("Warning!", "Please fill out this project name", "warning", {
        buttons: false,
        timer: 1200,
      })
    } else {
      const { project_name } = this.state
      const company_id = this.state.result.company
      const user_create = this.state.result.empID
      const obj = {
        project_name, company_id, user_create
      }

      try {
        await post('/insert_project', obj)
        swal("Success!", "Update Success", "success", {
          buttons: false,
          timer: 1200,
        }).then(() => {
          window.location.reload()
        })
      } catch (error) {
        swal("Error!", "Error", "error", {
          buttons: false,
          timer: 1200,
        })
      }
    }
  }
  ButtonApprove = (cell, Row) => {
    return (
      <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
        <img src={add} style={{ marginRight: 10, cursor: 'pointer' }} onClick={() => this.click(cell)} />
        <input type="button" value="Edit" className="del" onClick={() => this.onOpenModal2(cell)} />
      </div>
    )
  }
  click = async (cell) => {
    try {
      const res = await post('/show_project2', { project_id: cell })
      localStorage.setItem("job", JSON.stringify(res.result))
      this.props.history.push('/job')
    } catch (error) {
      swal("Error!", "Error", "error", {
        buttons: false,
        timer: 1200,
      })
    }
  }
  _onCreateProject = async () => {
    if (this.state.project_name == "" || this.state.project_start == "" || this.state.project_end == "") {
      swal("Warning!", "Please fill out this form", "warning", {
        buttons: false,
        timer: 1200,
      })
    }
    else {
      const project_name = this.state.project_name
      const project_start = moment(this.state.project_start).format('YYYY-MM-DD')
      const project_end = moment(this.state.project_end).format('YYYY-MM-DD')
      const user_create = this.state.result.empID
      const id = this.state.project_id
      const obj = {
        project_name, project_start, project_end, user_create, id
      }

      try {
        await post('/insert_project', obj)
        swal("Success!", "Success", "success", {
          buttons: false,
          timer: 1200,
        }).then(() => {
          window.location.reload()
        })
      } catch (error) {
        swal("Error!", "Error", "error", {
          buttons: false,
          timer: 1200,
        })
      }
    }
  }

  _onChangeProject = async () => {
    if (this.state.project_name == "" || this.state.project_start == "" || this.state.project_end == "") {
      swal("Warning!", "Please fill out this form", "warning", {
        buttons: false,
        timer: 1200,
      })
    }
    else {
      const project_name = this.state.project_name
      const project_start = moment(this.state.project_start).format('DD-MM-YYYY')
      const project_end = moment(this.state.project_end).format('DD-MM-YYYY')
      const user_create = this.state.result.empID
      const id = this.state.project_id
      const obj = {
        project_name, project_start, project_end, user_create, id
      }

      try {
        await post('/update_project', obj)
        swal("Success!", "Success", "success", {
          buttons: false,
          timer: 1200,
        }).then(() => {
          window.location.reload()
        })
      } catch (error) {
        swal("Error!", "Error", "error", {
          buttons: false,
          timer: 1200,
        })
      }
    }
  }

  _onChange = e => {
    this.setState({ [e.target.name]: e.target.value })

  }
  render() {
    const { open2, addProject } = this.state;
    return (
      <div>
        <div>

          <div className='company1'>
            <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
              <div style={{ display: '-webkit-inline-box' }}><h3 style={{ marginRight: 10 }}>Project</h3>
                <Link to='/AllPeople'><img src={swit} style={{ cursor: 'pointer' }} /></Link>
              </div>
            </div>
            <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
              <input type="button" value="Create" className="add" onClick={() => this.setState({ addProject: true })} />
            </div>
          </div>

          <div style={{ marginBottom: '20px' }}>
            <BootstrapTable className='boot' data={this.state.dataTables} hover search pagination>
              <TableHeaderColumn width={80} dataField='project_id' dataSort isKey>{'ID'}</TableHeaderColumn>
              <TableHeaderColumn dataField='project_name' dataSort>{'Project Name'}</TableHeaderColumn>
              <TableHeaderColumn dataField='project_start' dataSort dataFormat={(cell) => <p>{moment(cell).format('DD-MM-YYYY')}</p>}>{'Start'}</TableHeaderColumn>
              <TableHeaderColumn dataField='project_end' dataSort dataFormat={(cell) => <p>{moment(cell).format('DD-MM-YYYY')}</p>}>{'End'}</TableHeaderColumn>
              <TableHeaderColumn dataField='comname' dataSort>{'Company'}</TableHeaderColumn>
              <TableHeaderColumn dataField='userName' dataSort>{'User Create'}</TableHeaderColumn>
              <TableHeaderColumn width={150} dataField='project_id' dataSort dataFormat={this.ButtonApprove}>{}</TableHeaderColumn>
            </BootstrapTable>
          </div>
          <Modal open={open2} onClose={this.onCloseModal2} center className='ModalApprove' >
            <div className='cu1'>
              <div style={{ marginTop: 20, textAlign: 'center' }}>
                <h3 >Edit Project</h3></div>
            </div>
            <div className="App-User99">

              <input type="text" name="project_name" value={this.state.project_name} className="App-ubbbb" onChange={this._onChange} />
              <input type="date" name="project_start" value={moment(this.state.project_start).format('DD-MM-YYYY')} className="App-ubbbb" onChange={this._onChange} />
              <input type="date" name="project_end" value={moment(this.state.project_end).format('DD-MM-YYYY')} className="App-ubbbb" onChange={this._onChange} />

              <div flexDirection="row" className='cu2'>
                <input type="button" value="OK" className="App-button" onClick={this._onChangeProject} />
              </div>

            </div>
          </Modal>
          <Modal open={addProject} onClose={this.onCloseAddProject} center className='ModalApprove' >
            <div style={{ alignItems: 'center', justifyContent: 'center' }}>
              <ControlLabel style={{ marginTop: 10, marginBottom: 20, borderBottom: '2px solid #BDBDBD', padding: 10, fontSize: 20, textAlign: 'center' }}>Create Project</ControlLabel>
              <FormGroup
                controlId="formBasicText"
                validationState={this.getValidationAddProjectNameState()}
              >
                <FormControl
                  type="text"
                  name="comnames"
                  value={this.state.project_name}
                  placeholder="Enter Project Name"
                  onChange={this.handleAddProject}
                />
                <FormControl.Feedback />
              </FormGroup>
              <FormGroup
                controlId="formBasicText"
                validationState={this.getValidationProjectStart_State()}
              >
                <FormControl
                  type="date"
                  name="date_start"
                  placeholder="Enter Project Start"
                  value={this.state.project_start}
                  onChange={this.handleAddProjectStart}
                />
                <FormControl.Feedback />
              </FormGroup>
              <FormGroup
                controlId="formBasicText"
                validationState={this.getValidationProjectEnd_State()}
              >
                <FormControl
                  type="date"
                  name="date_end"
                  value={this.state.project_end}
                  onChange={this.handleAddProjectEnd}
                />
                <FormControl.Feedback />
              </FormGroup>

              <div flexDirection="row" className='cu2'>
                <input type="button" value="Create" className="App-button" onClick={this._onCreateProject} />
              </div>
            </div>
          </Modal>
        </div>
      </div>
    )
  }
}
