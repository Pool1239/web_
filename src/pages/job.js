import React, { Component } from 'react'
import '../pages/Leave.css'
import { get, post, sever } from '../service/api'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import moment from 'moment';
import searc from '../img/magnifying-glass.png'
import Modal from "react-responsive-modal"
import './company.css'
import add from '../img/magnifying-glass.png'
import { Link } from 'react-router-dom';
import swit from '../img/switch (2).png'

export default class job extends Component {

    constructor(props) {
        super(props)

        this.state = {
            result: {
                userName: ''
            },
            datatable: [],
            project_name: '',
            userdata: [],
            totalall: '',
            total: ''

        }
    }
    componentWillMount = () => {
        let res = localStorage.getItem('userdata');
        if (res) {
            this.setState({ result: JSON.parse(res)[0] })
        }
    }
    componentDidMount = async () => {
        if (this.state.result.userName) {
            const q = await get('/show_leave_request')
            if (q.success) {
                const res = await get('/project')
                if (res.success) {
                    let userdata = JSON.parse(localStorage.getItem("job"))
                    if (userdata) {
                        this.setState({ userdata })
                    } else {
                        this.setState({ userdata: [] })
                    }
                    const project_id = parseInt(userdata[0].project_id)
                    const obj = { project_id }
                    const ress = await post('/show_job', obj)
                    this.setState({ datatable: ress.result })
                    console.log(ress.result)
                    const totalall = await post('/totalall_time', obj)
                    this.setState({ totalall: totalall.result[0].total_time })
                    const total = await post('/total_time', obj)
                    this.setState({ total: total.result[0].total_time })

                } else {
                    this.props.history.push('/page2')
                    window.location.reload()
                }
            } else {
                this.props.history.push('/')
                window.location.reload()
            }
        }
        else {
            this.props.history.push('/')
            window.location.reload()
        }
    }
    render() {

        return (
            <div>
                <div>

                    <div className='company1'>
                        <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                            <div style={{ display: '-webkit-inline-box' }}>
                                <h3 style={{ marginRight: 10 }}>{this.state.userdata.length > 0 ? this.state.userdata[0].project_name : null}</h3>
                            </div>
                        </div>
                    </div>
                    <div style={{ marginBottom: '20px' }}>
                        <BootstrapTable className='boot' data={this.state.datatable} search hover pagination>
                            <TableHeaderColumn width={80} dataField='user_id' dataSort isKey>{'ID'}</TableHeaderColumn>
                            <TableHeaderColumn dataField='full_name' dataSort>{'Employee Name'}</TableHeaderColumn>
                            <TableHeaderColumn dataField='sum_time' dataSort dataFormat={(cell) => cell.toFixed(2).replace(".", ":") + '/' + this.state.total + ' ' + 'hr.'}>{'Hour'}</TableHeaderColumn>
                            <TableHeaderColumn dataField='total_time' dataSort dataFormat={(cell) => ((cell / this.state.totalall) * 100).toFixed(0) + '%'}>{'Percent'}</TableHeaderColumn>
                        </BootstrapTable>
                    </div>
                </div>
            </div>
        )
    }
}
