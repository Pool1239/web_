import React, { Component } from 'react';
import './Leave.css';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Button } from 'react-bootstrap'
import { Row } from 'antd';
import { Link } from 'react-router-dom';
import searc from '../img/magnifying-glass.png'
import { get } from '../service/api'
import { post } from '../service/api'
import moment from 'moment';
import swal from 'sweetalert';

export default class PageUser extends Component {
  constructor(props) {
    super(props)

    this.state = {
      dataTable: [],
      dataTable2: [],
      result: {
        userName: ''
      }
    }
  }
  componentWillMount = () => {
    let res = localStorage.getItem('userdata');
    if (res) {
      this.setState({ result: JSON.parse(res)[0] })
    }
  }
  componentDidMount = async () => {
    if (this.state.result.userName) {
      const q = await get('/show_leave_request')
      if (q.success) {
        if (this.state.result.company == 1) {
          const res = await get('/show_user')
          const ress = await get('/show_user2')
          if (res.success && ress.success) {
            this.setState({ dataTable: res.result })
            this.setState({ dataTable2: ress.result })
          } else {
            this.props.history.push('/page2')
            window.location.reload()
          }
        } else {
          const res = await post('/show_user_by_company', { company: this.state.result.company })
          const ress = await post('/show_user2_by_company', { company: this.state.result.company })
          if (res.success && ress.success) {
            this.setState({ dataTable: res.result })
            this.setState({ dataTable2: ress.result })
          } else {
            this.props.history.push('/page2')
            window.location.reload()
          }
        }
      }
      else {
        this.props.history.push('/')
        window.location.reload()
      }

    } else {
      this.props.history.push('/')
      window.location.reload()
    }
  }


  ButtonApprove = (cell, Row) => {
    return (
      <div>
        <input type="button" value="Delete" className="del" onClick={() => this.delete(cell)} />
      </div>
    )
  }

  delete = (cell) => {
    // console.log(cell)
    swal("Warning!", "Delete User", "warning", { buttons: [true, "Delete",] })
      .then(async success => {
        if (success) {
          try {
            await post('/delete_user', { empID: cell })
            swal("Success!", "Delete Success", "success", {
              buttons: false,
              timer: 1200,
            }).then(() => {
              window.location.reload()
            })
          } catch (error) {
            swal("Error!", "Error", "error", {
              buttons: false,
              timer: 1200,
            })
          }
        } else {

        }
      })
  }

  _status = (cell) => {
    return (
      <div style={{ color: cell === '4 months' ? 'red' : cell === 'Pass' ? 'green' : 'red' }} >
        {cell}
      </div>
    )
  }
  render() {
    return (
      <div>{this.state.result.company == 1 ?
        <div>
          <div className='lea'>
            <div><h3>User ({this.state.dataTable2.length > 0 ? this.state.dataTable2[0].sumid : null} users)</h3></div>
          </div>

          <div style={{ marginBottom: '20px' }} >
            {/* <BootstrapTable
          data={this.product} hover
          pagination>
          <TableHeaderColumn dataField='interval_value' isKey>Product ID</TableHeaderColumn>
          <TableHeaderColumn dataField='name'>Product Name</TableHeaderColumn>
          <TableHeaderColumn dataField='price'>Product Price</TableHeaderColumn>
        </BootstrapTable> */}
            <BootstrapTable search className='boot' data={this.state.dataTable} hover pagination>
              <TableHeaderColumn width='80' dataField='empID' dataSort isKey>{'ID'}</TableHeaderColumn>
              <TableHeaderColumn dataField='full_name' dataSort>{'Employee Name'}</TableHeaderColumn>
              <TableHeaderColumn dataField='email' dataSort>{'Email'}</TableHeaderColumn>
              <TableHeaderColumn width='175' dataField='starting_date' dataSort dataFormat={(cell) => <p>{moment(cell).format('DD-MM-YYYY  HH:MM:SS')}</p>}>{'Starting Date'}</TableHeaderColumn>
              <TableHeaderColumn width='130' dataField='pro_name' dataFormat={this._status} dataSort>{'Probation period'}</TableHeaderColumn>
              <TableHeaderColumn dataField='admin_email' dataSort>{'Approval Email'}</TableHeaderColumn>
              <TableHeaderColumn width='110' dataField='rol_name' dataSort>{'Role'}</TableHeaderColumn>
              <TableHeaderColumn width='150' dataField='comname' dataSort>{'Company'}</TableHeaderColumn>
              <TableHeaderColumn width='100' dataField='empID' dataSort dataFormat={this.ButtonApprove}>{''}</TableHeaderColumn>
            </BootstrapTable>
          </div>
        </div>
        :
        <div>
          <div className='lea'>
            <div><h3>User ({this.state.dataTable2.length > 0 ? this.state.dataTable2[0].sumid : null} users)</h3></div>
            <div><Link to='/create'>+ Create User Accout</Link></div>
          </div>

          <div style={{ marginBottom: '20px' }} >
            {/* <BootstrapTable
            data={this.product} hover
            pagination>
            <TableHeaderColumn dataField='interval_value' isKey>Product ID</TableHeaderColumn>
            <TableHeaderColumn dataField='name'>Product Name</TableHeaderColumn>
            <TableHeaderColumn dataField='price'>Product Price</TableHeaderColumn>
          </BootstrapTable> */}
            <BootstrapTable search className='boot' data={this.state.dataTable} hover pagination>
              <TableHeaderColumn width='80' dataField='empID' dataSort isKey>{'ID'}</TableHeaderColumn>
              <TableHeaderColumn dataField='full_name' dataSort>{'Employee Name'}</TableHeaderColumn>
              <TableHeaderColumn dataField='email' dataSort>{'Email'}</TableHeaderColumn>
              <TableHeaderColumn width='250' dataField='starting_date' dataSort dataFormat={(cell) => <p>{moment(cell).format('DD-MM-YYYY  HH:MM:SS')}</p>}>{'Starting Date'}</TableHeaderColumn>
              <TableHeaderColumn dataField='pro_name' dataFormat={this._status} dataSort>{'Probation period'}</TableHeaderColumn>
              <TableHeaderColumn dataField='admin_email' dataSort>{'Approval Email'}</TableHeaderColumn>
              <TableHeaderColumn width='150' dataField='rol_name' dataSort>{'Role'}</TableHeaderColumn>
              <TableHeaderColumn width='100' dataField='empID' dataSort dataFormat={this.ButtonApprove}>{''}</TableHeaderColumn>
            </BootstrapTable>
          </div>
        </div>
      }
      </div>
    );
  }
}
