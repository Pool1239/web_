import React, { Component } from 'react'
import '../pages/create.css'
import { post, get } from '../service/api'
import Modal from "react-responsive-modal"
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import swal from 'sweetalert';

export default class update_leave_type extends Component {
    constructor(props) {
        super(props)

        this.state = {
            edit: [],
            leaID_type: '',
            leaName_type: '',
            leaName_thai: '',
            max_dayy: [],
            max_day: '',
            showdata: '',
            result: {
                userName: ''
            }
        }
    }
    state = {
        open: false,
    };
    onOpenModal = async (cell) => {
        try {
            const res = await post('/show_max_day2', { leaID_type: cell })
            if (res.success) {
                this.setState({ open: true, leaName_thai: res.result[0].leaName_thai, leaName_type: res.result[0].leaName_type, max_day: res.result[0].max_day, leaID_type: res.result[0].leaID_type })
            } else {
                swal("Error!", res.message, "error", {
                    buttons: false,
                    timer: 1200,
                })
            }
        } catch (error) {
            swal("Error!", "Error", "error", {
                buttons: false,
                timer: 1200,
            })
        }
    }

    onCloseModal = () => {
        this.setState({ open: false })
        window.location.reload()
    }
    _onChange = e => {
        this.setState({ [e.target.name]: e.target.value })

    }
    componentWillMount = () => {
        let res = localStorage.getItem('userdata');
        if (res) {
            this.setState({ result: JSON.parse(res)[0] })
        }
    }
    componentDidMount = async () => {
        if (this.state.result.userName) {
            const q = await get('/show_leave_request')
            if (q.success) {
                try {
                    const res = await post('/show_max_day', { empID: this.state.result.empID })
                    this.setState({ max_dayy: res.result })
                } catch (error) {
                    alert(error)
                }
            } else {
                this.props.history.push('/')
                window.location.reload()
            }
        }
        else {
            this.props.history.push('/')
            window.location.reload()
        }
    }
    _onClick2 = async () => {
        window.location.reload()
    }
    _onClick = async () => {
        if (this.state.leaName_type == "" || this.state.leaName_thai == "" || this.state.max_day == "") {
            swal("Warning!", "Please fill out this form", "warning", {
                buttons: false,
                timer: 1200,
            })
        }
        else {

            const { leaName_type, leaName_thai, max_day } = this.state
            const leaID_type = this.state.leaID_type

            const obj = {
                leaName_type, leaName_thai, max_day, leaID_type
            }

            try {
                await post('/update_leave_type', obj)
                swal("Success!", "Update Success", "success", {
                    buttons: false,
                    timer: 1200,
                }).then(() => {
                    window.location.reload()
                })
            } catch (error) {
                swal("Error!", "Error", "error", {
                    buttons: false,
                    timer: 1200,
                })
            }
        }
    }
    ButtonApprove = (cell, Row) => {
        return (
            <div>
                <input type="button" value="Edit" className="del" onClick={() => this.onOpenModal(cell)} />
            </div>
        )
    }
    render() {
        const { open } = this.state;
        return (
            <div>
                <Modal open={open} onClose={this.onCloseModal} center className='ModalApprove' >
                    <div>
                        <div className='cu1'>
                            <h3 >Update Leave Type</h3>
                        </div>
                        <div className="App-User">

                            <input type="text" name="leaName_type" value={this.state.leaName_type} className="App-ub" onChange={this._onChange} />
                            <input type="text" name="leaName_thai" value={this.state.leaName_thai} className="App-ub" onChange={this._onChange} />
                            <input type="number" name="max_day" value={this.state.max_day} className="App-ub" onChange={this._onChange} />

                            <div flexDirection="row" className='cu2'>
                                <input type="button" value="Cancel" className="App-button1" onClick={this._onClick2} />
                                <input type="button" value="Create Account" className="App-button" onClick={this._onClick} />
                            </div>

                        </div>
                    </div>
                </Modal>
                <h3>
                    <div className='lea'>
                        <p>Update Leave Type</p>
                    </div>
                </h3>
                <BootstrapTable className='boot' data={this.state.max_dayy} hover pagination>
                    <TableHeaderColumn width={80} dataField='leaID_type' dataSort isKey>{'ID'}</TableHeaderColumn>
                    <TableHeaderColumn dataField='leaName_type' dataSort>{'Leave Name English'}</TableHeaderColumn>
                    <TableHeaderColumn dataField='leaName_thai' dataSort>{'Leave Name Thai'}</TableHeaderColumn>
                    <TableHeaderColumn width={100} dataField='max_day' dataSort>{'Max Day'}</TableHeaderColumn>
                    <TableHeaderColumn width={100} dataField='leaID_type' dataSort dataFormat={this.ButtonApprove} >{}</TableHeaderColumn>
                </BootstrapTable>

            </div >
        )
    }
}
