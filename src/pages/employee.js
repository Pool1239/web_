import React, { Component } from 'react'
import '../pages/employee.css'
import { post } from '../service/api'
import { TimePicker, Checkbox, DatePicker } from 'antd';
import 'antd/dist/antd.css';
import moment from 'moment';
import swal from 'sweetalert'
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;

///----------------------------------------------------///
// function onChange(time, timeString) {
//   console.log(time, timeString);
// }
// // function of timepicker line 83-90 

// function onChange(date, dateString) {
//   console.log(date, dateString);
// }
// function of date picker line 94

export default class employee extends Component {

  constructor(props) {
    super(props)

    this.state = {
      time_from: '',
      time_to: '',
      date_time: '',
      lea_type: '',
      lea_day: '1',
      date_start: '',
      date_end: '',
      lea_reason: '',
      userdatas: [],
      result: {
        userName: ''
      }
    };
    this.handleChange = this.handleChange.bind(this),
      this.handleChange2 = this.handleChange2.bind(this),
      this.handleChange3 = this.handleChange3.bind(this),
      this.handleChange4 = this.handleChange4.bind(this),
      this.handleChange5 = this.handleChange5.bind(this)
  }

  handleChange(date) {
    this.setState({
      date_start: date,
    });
  }
  handleChange2(date) {
    this.setState({
      date_end: date,
    });
  }
  handleChange3(date) {
    this.setState({
      date_time: date,
    });
  }
  handleChange4(date) {
    this.setState({
      time_from: date,
    });
  }
  handleChange5(date) {
    this.setState({
      time_to: date,
    });
  }

  _checkbox = event => {
    this.setState({ selectedValue: event.target.value });
  };

  _onChange = e => {
    this.setState({ [e.target.name]: e.target.value })
    // console.log(this.state.userName)
  }
  componentWillMount = () => {
    let res = localStorage.getItem('userdata');
    if (res) {
      console.log(JSON.parse(res)[0])
      this.setState({ result: JSON.parse(res)[0] })
    }
  }
  componentDidMount = async () => {
    if (this.state.result.userName) {
      let userdatas = JSON.parse(localStorage.getItem("an"))
      if (userdatas) {
        this.setState({ userdatas })
      } else {
        this.setState({ userdatas: [] })
      }
    }
    else {
      this.props.history.push('/')
      window.location.reload()
    }
  }

  render() {

    return (
      <div style={{ marginBottom: '50px' }}>

        <p className='em3'>
          <div className='p_headder'>Leave Request</div>
          <div className='em1'>
            Leave Request
        <div className="em"></div>
          </div>
        </p>
        <div className='emall'>
          <div className='block1'>
            <p> {this.state.userdatas.length > 0 ? this.state.userdatas[0].leaName_type : null} </p>
          </div>

          <div className='emall'>
            <select id="country" name="lea_day" placeholder="Type" className='em4' onChange={this._onChange}>
              <option value="1">Full Day</option>
              <option value="2">Half Day</option>
            </select>
          </div>
          {
            this.state.lea_day == 1 ?

              <div className='abcd'>
                <DatePicker selected={this.state.date_start} onChange={this.handleChange} placeholder='From' className='datepic1' />
                <DatePicker selected={this.state.date_end} onChange={this.handleChange2} placeholder='To' className='datepic2' />
              </div>


              // <div  >
              //   <div className='emall'>
              //     <input type="date" name="date_start" id="dateofbirth" placeholder="From" className='em6' onChange={this._onChange} />
              //     <input type="date" name="date_end" id="dateofbirth" placeholder="From" className='em6' onChange={this._onChange} />
              //   </div>
              // </div> 
              :
              <div >
                <div className='datepicker'>
                  <DatePicker selected={this.state.date_time} onChange={this.handleChange3} placeholder='Select Date' className='datepic' />

                </div>
                <div className='halfday01' >
                  <div><Checkbox style={{ width: '100px', fontWeight: 'normal', marginLeft: '3px' }} checked={this.state.selectedValue === 'a'} onChange={this._checkbox} value="a" name="radio-button-demo" aria-label="A">Morning</Checkbox></div>
                  <TimePicker selected={this.state.time_from} onChange={this.handleChange4} use24Hours format="h:mm a" placeholder={'from'} style={{ marginLeft: '4px', width: '115px', fontWeight: 'normal' }} />
                  <TimePicker selected={this.state.time_to} onChange={this.handleChange5} use24Hours format="h:mm a" placeholder={'to'} style={{ marginLeft: '10px', width: '115px', fontWeight: 'normal' }} />
                </div>

                <div className='halfday02'>
                  <div><Checkbox style={{ width: '100px', fontWeight: 'normal', marginLeft: '8px', borderRadius: '50%' }} checked={this.state.selectedValue === 'b'} onChange={this._checkbox} value="b" name="radio-button-demo" aria-label="B">Afternoon</Checkbox> </div>
                  <TimePicker selected={this.state.time_from} onChange={this.handleChange4} use24Hours format="h:mm a" placeholder={'from'} style={{ width: '115px', fontWeight: 'normal' }} />
                  <TimePicker selected={this.state.time_to} onChange={this.handleChange5} use24Hours format="h:mm a" placeholder={'to'} style={{ marginLeft: '10px', width: '115px', fontWeight: 'normal' }} />
                </div>


              </div>




            // <div>
            //   <div style={{ marginBottom: '10px' }}>
            //     <div className='ch' ><input type="time" className='em7' name="date_start" onChange={this._onChange} /></div>
            //     <div className='ch' ><input type="time" className='em7' name="date_start" onChange={this._onChange} /></div>
            //   </div>

            //   <div>

            //     <div className='time2' ><input type="time" className='em7' name="date_end" onChange={this._onChange} /> </div>
            //     <div className='time2' ><input type="time" className='em7' name="date_end" onChange={this._onChange} /> </div>
            //   </div>

            // </div>
          }
          <div className='emall'>
            <textarea name="lea_reason" placeholder="Leave Reason" className='em5' onChange={this._onChange} />
          </div>
          <div className='emall'>
            <button value="Cancle" className="embt" onClick={this._onClick2}> Cancle</button>
            <button value="Submit" className="embt" onClick={this._onClick} >Submit</button>
          </div>

        </div>
        <footer className="p_footer">ProjectSoft (Thailand).Co.,Ltd. </footer>
      </div>

    )
  }

  _onClick = async () => {
    if (this.state.lea_day == 1) {
      let userdata = JSON.parse(localStorage.getItem("userdata"))
      if (userdata) {
        this.setState({ userdata })
      } else {
        this.setState({ userdata: [] })
      }
      let userdatas = JSON.parse(localStorage.getItem("an"))
      if (userdatas) {
        this.setState({ userdatas })
      } else {
        this.setState({ userdatas: [] })
      }
      const { lea_day, lea_reason } = this.state
      const empID = parseInt(userdata[0].empID)
      const lea_type = parseInt(userdatas[0].leaID_type)
      const date_start = moment(this.state.date_start).format('DD-MM-YYYY HH:mm')
      const date_end = moment(this.state.date_end).format('DD-MM-YYYY HH:mm')
      const obj = {
        lea_type, lea_day, date_start, date_end, lea_reason, empID
      }

      try {
        await post('/insert_leave', obj)
        swal("Success!", "Create success", "success", {
          buttons: false,
          timer: 1200,
        }).then(() => {
          window.location.reload()
        })
      } catch (error) {
        swal("Error!", "Error", "error", {
          buttons: false,
          timer: 1200,
        })
      }
    }
    else {
      let userdata = JSON.parse(localStorage.getItem("userdata"))
      if (userdata) {
        this.setState({ userdata })
      } else {
        this.setState({ userdata: [] })
      }
      let userdatas = JSON.parse(localStorage.getItem("an"))
      if (userdatas) {
        this.setState({ userdatas })
      } else {
        this.setState({ userdatas: [] })
      }
      const { lea_day, lea_reason } = this.state
      const empID = parseInt(userdata[0].empID)
      const lea_type = parseInt(userdatas[0].leaID_type)
      const date_start = moment(this.state.date_time).format('DD-MM-YYYY') + ' ' + moment(this.state.time_from).format('HH:mm')
      const date_end = moment(this.state.date_time).format('DD-MM-YYYY') + ' ' + moment(this.state.time_to).format('HH:mm')
      const obj = {
        lea_type, lea_day, date_start, date_end, lea_reason, empID
      }
      try {
        await post('/insert_leave', obj)
        swal("Success!", "Create Success", "success", {
          buttons: false,
          timer: 1200,
        }).then(() => {
          window.location.reload()
        })
      } catch (error) {
        swal("Error!", "Error", "error", {
          buttons: false,
          timer: 1200,
        })
      }
    }
  }
  _onClick2 = async () => {
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    if (userdata) {
      this.setState({ userdata })
      if (userdata[0].role_id == 1) {
        this.props.history.push('/leaverequest')
      } else {
        this.props.history.push('/LeaveRequest')
      }
    } else {
      this.setState({ userdata: [] })
    }
  }
}

