import React, { Component } from 'react'
import '../pages/Leave.css'
import { get, post, sever } from '../service/api'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import moment from 'moment';
import searc from '../img/magnifying-glass.png'
import Modal from "react-responsive-modal"
import './company.css'
import add from '../img/magnifying-glass.png'
import { Link } from 'react-router-dom';
import swit from '../img/switch (1).png';
import swal from 'sweetalert'

export default class AllPeople extends Component {
  constructor(props) {
    super(props)

    this.state = {
      dataTable: [],
      dataTable2: [],
      result: {
        userName: ''
      }
    }
  }
  state = {
    open: false,
  };

  componentWillMount = () => {
    let res = localStorage.getItem('userdata');
    if (res) {
      this.setState({ result: JSON.parse(res)[0] })
    }
  }
  componentDidMount = async () => {
    if (this.state.result.userName) {
      if (this.state.result.company == 1) {
        const res = await get('/show_checkin')
        if (res.success) {
          this.setState({ dataTable: res.result })

        } else {
          this.props.history.push('/LeaveRequest')
          window.location.reload()
        }
      } else {
        const res = await post('/show_checkin2', { company: this.state.result.company })
        if (res.success) {
          this.setState({ dataTable: res.result })
        } else {
          this.props.history.push('/LeaveRequest')
          window.location.reload()
        }
      }

    } else {
      this.props.history.push('/')
      window.location.reload()
    }
  }


  ButtonApprove = (cell, Row) => {
    return (
      <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
        <img src={add} style={{ marginRight: 10, cursor: 'pointer' }} onClick={() => this.click(Row)} />
      </div>
    )
  }
  click = async (Row) => {
    try {
      const date = moment(Row.date).format('DD-MM-YYYY')
      const user_id = Row.empID
      const obj = {
        date, user_id
      }

      const res = await post('/show_job_2', obj)
      localStorage.setItem("job3", JSON.stringify(res.result))
      this.props.history.push('/job2')
    } catch (error) {
      swal("Error!", "Error", "error", {
        buttons: false,
        timer: 1200,
      })
    }
  }

  render() {

    return (
      <div>
        <div>
          <div className='company1'>
            <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
              <div style={{ display: '-webkit-inline-box' }}><h3 style={{ marginRight: 10 }}>People</h3>
                <Link to='/AllProject'><img src={swit} style={{ cursor: 'pointer' }} /></Link>
              </div>
            </div>

          </div>

          <div style={{ marginBottom: '20px' }}>
            <BootstrapTable search className='boot' data={this.state.dataTable} hover pagination>
              <TableHeaderColumn width='80' dataField='checkin_id' dataSort isKey>{'ID'}</TableHeaderColumn>
              <TableHeaderColumn dataField='full_name' dataSort>{'Employee Name'}</TableHeaderColumn>
              <TableHeaderColumn dataField='date' dataSort dataFormat={(cell) => <p>{moment(cell).format('DD-MM-YYYY')}</p>}>{'Date'}</TableHeaderColumn>
              <TableHeaderColumn width='130' dataField='date_in' dataSort >{'Time IN'}</TableHeaderColumn>
              <TableHeaderColumn width='130' dataField='date_out' dataSort>{'Time OUT'}</TableHeaderColumn>
              <TableHeaderColumn width='110' dataField='time_in_break' dataSort>{'Break IN'}</TableHeaderColumn>
              <TableHeaderColumn width='110' dataField='time_out_break' dataSort>{'Break OUT'}</TableHeaderColumn>
              <TableHeaderColumn width='150' dataField='full_times' dataSort>{'Working Hours'}</TableHeaderColumn>
              <TableHeaderColumn width='150' dataField='comname' dataSort>{'Company'}</TableHeaderColumn>
              <TableHeaderColumn width='100' dataField='empID' dataSort dataFormat={this.ButtonApprove}>{''}</TableHeaderColumn>
            </BootstrapTable>
          </div>
        </div>
      </div>
    )
  }
}