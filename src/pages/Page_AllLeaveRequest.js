import React, { Component } from 'react';
import './Leave.css';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Button } from 'antd/lib/radio';
import { Row } from 'antd';
import searc from '../img/magnifying-glass.png'
import { get } from '../service/api'
import { post } from '../service/api'
import moment from 'moment';
import Modal from "react-responsive-modal"
import swal from 'sweetalert';

export default class AllLeaveRequest extends Component {
  constructor(props) {
    super(props)

    this.state = {
      dataTables: [], dataTables2: [],
      showdata: [],
      result: {
        userName: ''
      }
    }
  }
  state = {
    open: false,
  };

  onOpenModal = async (cell) => {
    console.log(cell)
    try {
      const res = await post('/show_request', { leaveID: cell })
      if (res.success) {
        this.setState({ open: true, showdata: res.result })
      } else {
        swal("Error!", res.message, "error", {
          buttons: false,
          timer: 1200,
        })
      }

    } catch (error) {
      swal("Error!", "Error", "error", {
        buttons: false,
        timer: 1200,
      })
    }
  }

  onCloseModal = () => {
    this.setState({ open: false })
  }

  componentWillMount = () => {
    let res = localStorage.getItem('userdata');
    if (res) {
      console.log(JSON.parse(res)[0])
      this.setState({ result: JSON.parse(res)[0] })
    }

  }


  componentDidMount = async () => {
    if (this.state.result.role_id) {
      if (this.state.result.company == 1) {
        const res = await get('/show_leave_request')
        if (res.success) {
          console.log(res.result)
          this.setState({ dataTables: res.result })
        } else {
          this.props.history.push('/')
          window.location.reload()
        }
      }
      else {
        const res = await post('/show_leave_request_by_company', { company: this.state.result.company })
        if (res.success) {
          this.setState({ dataTables: res.result })
        } else {
          this.props.history.push('/')
          window.location.reload()
        }
      }

    } else {
      this.props.history.push('/')
      window.location.reload()
    }
  }

  ButtonApprove = (cell, Row) => {
    return (
      <div style={{ display: 'inline-flex' }}>
        {/* <div>
          <img src={searc} style={{ marginRight: 10, cursor: 'pointer' }} onClick={() => this.onOpenModal(cell)} />
        </div> */}
        <div
          onClick={() => this.onOpenModal(cell)}
        >
          {
            Row.sta_name == 'Wait' ?
              <button className='bbb' >Approve</button>
              :
              Row.sta_name == 'Approve' ?
                <button className='bbb2' disabled >Approve</button>
                :
                <button disabled className='bbb3' >Reject</button>
          }
        </div>
      </div>
    )
  }


  _status = (cell) => {
    return (
      <div style={{ color: cell === 'Wait' ? '#05cdff' : cell === 'Approve' ? 'green' : 'red', fontWeight: cell === 'Wait' ? null : 500 }} >
        {cell}
      </div>
    )
  }

  onUpdate = async (cell, leaveID) => {
    console.log(cell)
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    try {
      await post('/update_leave_request', { add_empID: userdata[0].empID, leaveID: cell })
      swal("Success!", "Update Success", "success", {
        buttons: false,
        timer: 1200,
      }).then(() => {
        window.location.reload()
      })
    } catch (error) {
      swal("Error!", "Error", "error", {
        buttons: false,
        timer: 1200,
      })
    }
  }

  onUpdate_1 = async (leaveID) => {
    console.log(leaveID)
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    try {
      await post('/update_leave_request_1', { add_empID: userdata[0].empID, leaveID })
      swal("Success!", "Update Success", "success", {
        buttons: false,
        timer: 1200,
      }).then(() => {
        window.location.reload()
      })
    } catch (error) {
      swal("Error!", "Error", "error", {
        buttons: false,
        timer: 1200,
      })
    }
  }

  render() {

    const { open } = this.state;
    return (

      <div>
        <h3>
          <div className='lea'>
            <p>Leave Request</p>
          </div>
        </h3>
        <div style={{ marginBottom: '20px' }}>
          {/* <BootstrapTable
            data={this.product} hover
            pagination>
            <TableHeaderColumn dataField='interval_value' isKey>Product ID</TableHeaderColumn>
            <TableHeaderColumn dataField='name'>Product Name</TableHeaderColumn>
            <TableHeaderColumn dataField='price'>Product Price</TableHeaderColumn>
          </BootstrapTable> */}
          <BootstrapTable className='boot' data={this.state.dataTables} hover pagination>
            <TableHeaderColumn width={40} dataField='Empid' dataSort isKey>{'ID'}</TableHeaderColumn>
            <TableHeaderColumn dataField='full_name' dataSort>{'Employee Name'}</TableHeaderColumn>
            <TableHeaderColumn width={130} dataField='comname' dataSort >{'Company Name'}</TableHeaderColumn>
            <TableHeaderColumn dataField='leaName_type' dataSort>{'Leave Type'}</TableHeaderColumn>
            <TableHeaderColumn width={80} dataField='leaName_day' dataSort>{'Type'}</TableHeaderColumn>
            <TableHeaderColumn width={120} dataField='date_start' dataSort dataFormat={(cell) => <p>{moment(cell).format('DD-MM-YYYY')}</p>}>{'From'}</TableHeaderColumn>
            <TableHeaderColumn width={120} dataField='date_end' dataSort dataFormat={(cell) => <p>{moment(cell).format('DD-MM-YYYY')}</p>}>{'To'}</TableHeaderColumn>
            <TableHeaderColumn width={120} dataField='full_day' dataSort >{'No. of days'}</TableHeaderColumn>
            <TableHeaderColumn width={80} dataField='sta_name' dataSort dataFormat={this._status} >{'Status'}</TableHeaderColumn>
            <TableHeaderColumn width={80} dataField='nameuser' dataSort>{'Approver'}</TableHeaderColumn>
            <TableHeaderColumn width={110} dataField='date_created' dataSort dataSort dataFormat={(cell) => <p>{moment(cell).format('DD-MM-YYYY')}</p>}>{'Created'}</TableHeaderColumn>
            <TableHeaderColumn dataField='leaveID' dataSort dataFormat={this.ButtonApprove} >{'Action'}</TableHeaderColumn>
          </BootstrapTable>
        </div>

        <Modal open={open} onClose={this.onCloseModal} center className='ModalApprove' >
          <div className='headerModal'>
            <p className='textheadermodal'>{this.state.showdata.length > 0 ? this.state.showdata[0].rol_name : null}</p>
            <div style={{ fontSize: 'xx-large' }}>{this.state.showdata.length > 0 ? this.state.showdata[0].full_name : null}</div>
          </div>

          <div className='headerModal2'>
            <p className='textheadermodal2'>Leave Request</p>
          </div>

          <div className='solidmodal' />

          <p className='nameModal'>{this.state.showdata.length > 0 ? this.state.showdata[0].leaName_type : null}</p>
          <p className='nameModal'>{this.state.showdata.length > 0 ? this.state.showdata[0].leaName_day : null}</p>

          <div className='datemodalclass'>

            <p className='dateModal1'>{moment(this.state.showdata.length > 0 ? this.state.showdata[0].date_start : null).format('DD-MM-YYYY')}</p>
            <p className='dateModal2'>{moment(this.state.showdata.length > 0 ? this.state.showdata[0].date_end : null).format('DD-MM-YYYY')}</p>

          </div>

          <p className='ModalReason'>{this.state.showdata.length > 0 ? this.state.showdata[0].lea_reason : null}</p>

          <div flexDirection="row" className='buttonModal1'>

            <button className='reject' onClick={() => this.onUpdate_1(this.state.showdata[0].leaveID)}>Reject</button>

            <button className='bb' onClick={() => this.onUpdate(this.state.showdata[0].leaveID)}>Approve</button>

          </div>
        </Modal>
      </div>

    );
  }
}