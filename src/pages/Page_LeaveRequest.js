import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Nav, NavItem, NavDropdown, Navbar, MenuItem, Button } from 'react-bootstrap'
import './Page_LeaveRequest.css'
import './ps.css'
import G7 from '../img/Group 7.png'
import { get, post, sever } from '../service/api'
import Modal from "react-responsive-modal"
import FileBase64 from 'react-file-base64'


export default class PageLeaveRequest extends Component {
  constructor(props) {
    super(props)

    this.state = {
      annual: [], timeannual: [], sumannual: '',
      personal: [], sumpersonal: '', timepersonal: [],
      sick: [], sumsick: '', timesickl: [],
      enter: [], sumenter: '', timeenter: [],
      military: [], summilitary: '', timemilitary: [],
      maternity: [], summaternity: '', timematernity: [],
      training: [], sumtraining: '', timetraining: [],
      userdata: [],
      files: [],
      max_day: [],
      result: {
        userName: ''
      },
      max_day: [],
      countrequest_type: '',
      leaName_type: '',
      leaName_thai: '',
      max_day: '',
      IDcompany: '',
      showdata: [],
    }
  }
  onOpenModal = async () => {
    this.setState({ open: true })
  }

  onCloseModal = () => {
    this.setState({ open: false })
    window.location.reload()
  }
  getFiles(files) {
    this.setState({ files: files, imagePreviewUrl: files[0].base64 })
    console.log(files[0].base64)
  }
  _onClick9 = async () => {

    let userdata = JSON.parse(localStorage.getItem("userdata"))

    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    const { files } = this.state

    const empID = parseInt(userdata[0].empID)
    const obj = {
      empID, image: files[0].base64
    }
    console.log(obj)


    try {
      await post('/insert_profile', obj)
      window.location.reload()
    } catch (error) {
      alert(error)
    }
  }
  componentWillMount = () => {
    let res = localStorage.getItem('userdata');
    if (res) {
      console.log(JSON.parse(res)[0])
      this.setState({ result: JSON.parse(res)[0] })
    }
  }
  componentDidMount = async () => {
    if (this.state.result.userName) {

      let userdata = JSON.parse(localStorage.getItem("userdata"))
      this.setState({ userdata })

      try {
        const res = await post('/show_request_count_company', { empID: userdata[0].empID })
        this.setState({ countrequest_type: res.result[0].countcompany })
      } catch (error) {
        alert(error)
      }

      try {
        const res = await post('/show_max_day', { empID: userdata[0].empID })
        this.setState({ max_day: res.result })
      } catch (error) {
        alert(error)
      }

      if (this.state.countrequest_type == 1) {
        try {
          const res = await post('/show_my_leave_request_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
          this.setState({ annual: res.result })
          const req = await post('/show_my_leave_request_1_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
          this.setState({ timeannual: req.result })
          this.setState({ sumannual: parseFloat(this.state.annual) + parseFloat(this.state.timeannual) })
        } catch (error) {
          alert(error)
        }
      } else if (this.state.countrequest_type == 2) {
        try {
          const res = await post('/show_my_leave_request_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
          this.setState({ annual: res.result })
          const req = await post('/show_my_leave_request_1_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
          this.setState({ timeannual: req.result })
          this.setState({ sumannual: parseFloat(this.state.annual) + parseFloat(this.state.timeannual) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_2', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
          this.setState({ personal: res.result })
          const req = await post('/show_my_leave_request_2_1', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
          this.setState({ timepersonal: req.result })
          this.setState({ sumpersonal: parseFloat(this.state.personal) + parseFloat(this.state.timepersonal) })
        } catch (error) {
          alert(error)
        }
      } else if (this.state.countrequest_type == 3) {
        try {
          const res = await post('/show_my_leave_request_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
          this.setState({ annual: res.result })
          const req = await post('/show_my_leave_request_1_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
          this.setState({ timeannual: req.result })
          this.setState({ sumannual: parseFloat(this.state.annual) + parseFloat(this.state.timeannual) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_2', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
          this.setState({ personal: res.result })
          const req = await post('/show_my_leave_request_2_1', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
          this.setState({ timepersonal: req.result })
          this.setState({ sumpersonal: parseFloat(this.state.personal) + parseFloat(this.state.timepersonal) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_3', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
          this.setState({ sick: res.result })
          const req = await post('/show_my_leave_request_3_1', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
          this.setState({ timesickl: req.result })
          this.setState({ sumsick: parseFloat(this.state.sick) + parseFloat(this.state.timesickl) })
        } catch (error) {
          alert(error)
        }
      } else if (this.state.countrequest_type == 4) {
        try {
          const res = await post('/show_my_leave_request_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
          this.setState({ annual: res.result })
          const req = await post('/show_my_leave_request_1_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
          this.setState({ timeannual: req.result })
          this.setState({ sumannual: parseFloat(this.state.annual) + parseFloat(this.state.timeannual) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_2', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
          this.setState({ personal: res.result })
          const req = await post('/show_my_leave_request_2_1', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
          this.setState({ timepersonal: req.result })
          this.setState({ sumpersonal: parseFloat(this.state.personal) + parseFloat(this.state.timepersonal) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_3', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
          this.setState({ sick: res.result })
          const req = await post('/show_my_leave_request_3_1', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
          this.setState({ timesickl: req.result })
          this.setState({ sumsick: parseFloat(this.state.sick) + parseFloat(this.state.timesickl) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_4', { empID: userdata[0].empID, lea_type: this.state.max_day[3].leaID_type })
          this.setState({ enter: res.result })
          const req = await post('/show_my_leave_request_4_1', { empID: userdata[0].empID, lea_type: this.state.max_day[3].leaID_type })
          this.setState({ timeenter: req.result })
          this.setState({ sumenter: parseFloat(this.state.enter) + parseFloat(this.state.timeenter) })
        } catch (error) {
          alert(error)
        }
      } else if (this.state.countrequest_type == 5) {
        try {
          const res = await post('/show_my_leave_request_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
          this.setState({ annual: res.result })
          const req = await post('/show_my_leave_request_1_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
          this.setState({ timeannual: req.result })
          this.setState({ sumannual: parseFloat(this.state.annual) + parseFloat(this.state.timeannual) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_2', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
          this.setState({ personal: res.result })
          const req = await post('/show_my_leave_request_2_1', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
          this.setState({ timepersonal: req.result })
          this.setState({ sumpersonal: parseFloat(this.state.personal) + parseFloat(this.state.timepersonal) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_3', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
          this.setState({ sick: res.result })
          const req = await post('/show_my_leave_request_3_1', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
          this.setState({ timesickl: req.result })
          this.setState({ sumsick: parseFloat(this.state.sick) + parseFloat(this.state.timesickl) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_4', { empID: userdata[0].empID, lea_type: this.state.max_day[3].leaID_type })
          this.setState({ enter: res.result })
          const req = await post('/show_my_leave_request_4_1', { empID: userdata[0].empID, lea_type: this.state.max_day[3].leaID_type })
          this.setState({ timeenter: req.result })
          this.setState({ sumenter: parseFloat(this.state.enter) + parseFloat(this.state.timeenter) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_5', { empID: userdata[0].empID, lea_type: this.state.max_day[4].leaID_type })
          this.setState({ military: res.result })
          const req = await post('/show_my_leave_request_5_1', { empID: userdata[0].empID, lea_type: this.state.max_day[4].leaID_type })
          this.setState({ timemilitary: req.result })
          this.setState({ summilitary: parseFloat(this.state.military) + parseFloat(this.state.timemilitary) })
        } catch (error) {
          alert(error)
        }
      } else if (this.state.countrequest_type == 6) {
        try {
          const res = await post('/show_my_leave_request_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
          this.setState({ annual: res.result })
          const req = await post('/show_my_leave_request_1_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
          this.setState({ timeannual: req.result })
          this.setState({ sumannual: parseFloat(this.state.annual) + parseFloat(this.state.timeannual) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_2', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
          this.setState({ personal: res.result })
          const req = await post('/show_my_leave_request_2_1', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
          this.setState({ timepersonal: req.result })
          this.setState({ sumpersonal: parseFloat(this.state.personal) + parseFloat(this.state.timepersonal) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_3', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
          this.setState({ sick: res.result })
          const req = await post('/show_my_leave_request_3_1', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
          this.setState({ timesickl: req.result })
          this.setState({ sumsick: parseFloat(this.state.sick) + parseFloat(this.state.timesickl) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_4', { empID: userdata[0].empID, lea_type: this.state.max_day[3].leaID_type })
          this.setState({ enter: res.result })
          const req = await post('/show_my_leave_request_4_1', { empID: userdata[0].empID, lea_type: this.state.max_day[3].leaID_type })
          this.setState({ timeenter: req.result })
          this.setState({ sumenter: parseFloat(this.state.enter) + parseFloat(this.state.timeenter) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_5', { empID: userdata[0].empID, lea_type: this.state.max_day[4].leaID_type })
          this.setState({ military: res.result })
          const req = await post('/show_my_leave_request_5_1', { empID: userdata[0].empID, lea_type: this.state.max_day[4].leaID_type })
          this.setState({ timemilitary: req.result })
          this.setState({ summilitary: parseFloat(this.state.military) + parseFloat(this.state.timemilitary) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_6', { empID: userdata[0].empID, lea_type: this.state.max_day[5].leaID_type })
          this.setState({ maternity: res.result })
          const req = await post('/show_my_leave_request_6_1', { empID: userdata[0].empID, lea_type: this.state.max_day[5].leaID_type })
          this.setState({ timematernity: req.result })
          this.setState({ summaternity: parseFloat(this.state.maternity) + parseFloat(this.state.timematernity) })
        } catch (error) {
          alert(error)
        }
      } else if (this.state.countrequest_type == 7) {
        try {
          const res = await post('/show_my_leave_request_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
          this.setState({ annual: res.result })
          const req = await post('/show_my_leave_request_1_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
          this.setState({ timeannual: req.result })
          this.setState({ sumannual: parseFloat(this.state.annual) + parseFloat(this.state.timeannual) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_2', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
          this.setState({ personal: res.result })
          const req = await post('/show_my_leave_request_2_1', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
          this.setState({ timepersonal: req.result })
          this.setState({ sumpersonal: parseFloat(this.state.personal) + parseFloat(this.state.timepersonal) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_3', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
          this.setState({ sick: res.result })
          const req = await post('/show_my_leave_request_3_1', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
          this.setState({ timesickl: req.result })
          this.setState({ sumsick: parseFloat(this.state.sick) + parseFloat(this.state.timesickl) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_4', { empID: userdata[0].empID, lea_type: this.state.max_day[3].leaID_type })
          this.setState({ enter: res.result })
          const req = await post('/show_my_leave_request_4_1', { empID: userdata[0].empID, lea_type: this.state.max_day[3].leaID_type })
          this.setState({ timeenter: req.result })
          this.setState({ sumenter: parseFloat(this.state.enter) + parseFloat(this.state.timeenter) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_5', { empID: userdata[0].empID, lea_type: this.state.max_day[4].leaID_type })
          this.setState({ military: res.result })
          const req = await post('/show_my_leave_request_5_1', { empID: userdata[0].empID, lea_type: this.state.max_day[4].leaID_type })
          this.setState({ timemilitary: req.result })
          this.setState({ summilitary: parseFloat(this.state.military) + parseFloat(this.state.timemilitary) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_6', { empID: userdata[0].empID, lea_type: this.state.max_day[5].leaID_type })
          this.setState({ maternity: res.result })
          const req = await post('/show_my_leave_request_6_1', { empID: userdata[0].empID, lea_type: this.state.max_day[5].leaID_type })
          this.setState({ timematernity: req.result })
          this.setState({ summaternity: parseFloat(this.state.maternity) + parseFloat(this.state.timematernity) })
        } catch (error) {
          alert(error)
        }
        try {
          const res = await post('/show_my_leave_request_7', { empID: userdata[0].empID, lea_type: this.state.max_day[6].leaID_type })
          this.setState({ training: res.result })
          const req = await post('/show_my_leave_request_7_1', { empID: userdata[0].empID, lea_type: this.state.max_day[6].leaID_type })
          this.setState({ timetraining: req.result })
          this.setState({ sumtraining: parseFloat(this.state.training) + parseFloat(this.state.timetraining) })
        } catch (error) {
          alert(error)
        }
      }
    } else {
      this.props.history.push('/')
      window.location.reload()
    }
  }
  _onClick = async () => {
    const an = this.state.max_day[0].leaID_type
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick2 = async () => {
    const an = this.state.max_day[1].leaID_type
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick3 = async () => {
    const an = this.state.max_day[2].leaID_type
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick4 = async () => {
    const an = this.state.max_day[3].leaID_type
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick5 = async () => {
    const an = this.state.max_day[4].leaID_type
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick6 = async () => {
    const an = this.state.max_day[5].leaID_type
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick7 = async () => {
    const an = this.state.max_day[6].leaID_type
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick8 = async () => {

    this.props.history.push('/flexible')

  }
  logout = async () => {
    localStorage.removeItem('userdata')
    this.props.history.push('/')
    window.location.reload()
  }
  clickk = async () => {
    this.props.history.push('/checkworking')
    window.location.reload()
  }

  render() {
    const { open } = this.state;
    let { imagePreviewUrl } = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<img className='imgUpload' src={imagePreviewUrl} />);
    } else {
      $imagePreview = (<div className="previewText">Upload Receipt</div>);
    }
    return (
      <div>
        <div className='p_headder'>
          <div class="col-md-4" style={{ textAlign: 'left', fontSize: '15px', cursor: 'pointer', paddingTop: '10px', height: '100%', marginTop: '6px' }} onClick={this.clickk}>Add Working </div>
          <div class="col-md-4" style={{ textAlign: 'center', fontWeight: 'bold', marginTop: '6px' }}>Leave Request & Remaining </div>
          <div class="col-md-4" onClick={this.logout} style={{ textAlign: 'right', fontSize: '15px', paddingTop: '10px', height: '100%', cursor: 'pointer' }}>
            <p className='lo'>Logout</p> </div>
        </div>

        <div style={{ height: '-webkit-fill-available', backgroundColor: '#f3f3f3', display: 'flex', justifyContent: 'center', flexDirection: 'column', alignItems: 'center' }} >
          <Modal open={open} onClose={this.onCloseModal} center className='ModalApprove' >
            <div className='headerModal'>
              <p className='textheadermodall'>เปลี่ยนรูปโปรไฟล์</p>
            </div>
            <div className='solidmodal123' />
            <div style={{ textAlign: 'center' }}>
              <div style={{ display: 'flex', justifyContent: 'center', flexDirection: 'column' }}>
                <div>{$imagePreview}</div>
                <p className='showww'><FileBase64 width={'100px'} multiple={true} onDone={this.getFiles.bind(this)} /> </p>
              </div>
              <div style={{ marginTop: '20px' }}>
                <input type="button" value="Cancle" className="App-button1" onClick={this.onCloseModal} />
                <input type="button" value="Enter" className="App-button" onClick={this._onClick9} />
              </div>
            </div>
          </Modal>


          {/* <div className='justify-center'> */}


          {this.state.countrequest_type == 0 ?
            <div style={{ width: '800px' }} >
              <div className='navbarrightsss1'>
                <div className="dro"><img className='dro2' src={sever + '/user_profiles'} onClick={this.onOpenModal} /> </div>
                <div className="dropbtn1" >
                  <p style={{ fontWeight: 'bold', color: '#05cdff', margin: 'unset', textAlign: 'center' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].role : null}</p>
                  <h4 style={{ color: 'black' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].full_name : null}</h4>
                </div>
              </div>

              <div className='col-md-12'>
                <div style={{ textAlign: '-webkit-center' }}>
                  <div className='solid'></div>
                </div>
                <div style={{ textAlign: 'center' }}><h3 className='h33'>Public Holiday</h3></div>
              </div>
            </div>
            :
            this.state.countrequest_type == 1 ?
              <div style={{ width: '800px' }} >
                <div className='navbarrightsss1'>
                  <div className="dro"><img className='dro2' src={sever + '/user_profiles'} onClick={this.onOpenModal} /> </div>
                  <div className="dropbtn1" >
                    <p style={{ fontWeight: 'bold', color: '#05cdff', margin: 'unset', textAlign: 'center' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].role : null}</p>
                    <h4 style={{ color: 'black' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].full_name : null}</h4>
                  </div>
                </div>

                <div className='col-md-12'>
                  <div style={{ textAlign: '-webkit-center' }}>
                    <div className='solid'></div>
                  </div>
                  <div><h3 className='h33'>Public Holiday</h3></div>

                  <div className='col-md-6'>
                    <div className='col10' onClick={this._onClick}>
                      <div className='colleft'> <p style={{ fontWeight: 'bold' }}> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_type : null}</p> <p> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_thai : null} </p> </div>
                      <div className='contentright'>
                        <div>
                          <span style={{ marginLeft: 15 }}> {this.state.sumannual}/{this.state.max_day.length > 0 ? this.state.max_day[0].max_day : null} </span>
                        </div>
                        <label style={{ marginRight: 6.5 }}>Days</label>
                      </div>
                    </div>
                  </div>

                  <div className='col-md-6'>
                    <div className='col10' onClick={this._onClick8}>
                      <div className='colleft1'><p style={{ fontWeight: 'bold' }}>Flexible Benefit Request</p> <p> คำขอเกี่ยวกับสวัสดิการอื่นๆ</p></div>
                      {/* <div className='colright'><p> </p></div> */}
                    </div>
                  </div>
                </div>
              </div>
              :
              this.state.countrequest_type == 2 ?
                <div style={{ width: '800px' }} >
                  <div className='navbarrightsss1'>
                    <div className="dro"><img className='dro2' src={sever + '/user_profiles'} onClick={this.onOpenModal} /> </div>
                    <div className="dropbtn1" >
                      <p style={{ fontWeight: 'bold', color: '#05cdff', margin: 'unset', textAlign: 'center' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].role : null}</p>
                      <h4 style={{ color: 'black' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].full_name : null}</h4>
                    </div>
                  </div>


                  <div className='col-md-12'>
                    <div style={{ textAlign: '-webkit-center' }}>
                      <div className='solid'></div>
                    </div>
                    <div><h3 className='h33'>Public Holiday</h3></div>

                    <div className='col-md-6'>

                      <div className='col10' onClick={this._onClick}>
                        <div className='colleft'> <p style={{ fontWeight: 'bold' }}> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_type : null}</p> <p> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_thai : null} </p> </div>
                        <div className='contentright'>
                          <div>
                            <span style={{ marginLeft: 15 }}> {this.state.sumannual}/{this.state.max_day.length > 0 ? this.state.max_day[0].max_day : null} </span>
                          </div>
                          <label style={{ marginRight: 6.5 }}>Days</label>
                        </div>
                      </div>
                    </div>

                    <div className='col-md-6'>
                      <div className='col10' onClick={this._onClick2}>
                        <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[1].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[1].leaName_thai : null}</p></div>
                        <div className='contentright'>
                          <div>
                            <span style={{ marginLeft: 15 }}>{this.state.sumpersonal}/{this.state.max_day.length > 0 ? this.state.max_day[1].max_day : null}</span></div>
                          <label style={{ marginRight: 6.5 }}>Days</label>
                        </div>
                      </div>
                    </div>

                    <div className='col-md-6'>
                      <div className='col10' onClick={this._onClick8}>
                        <div className='colleft1'><p style={{ fontWeight: 'bold' }}>Flexible Benefit Request</p> <p> คำขอเกี่ยวกับสวัสดิการอื่นๆ</p></div>
                        {/* <div className='colright'><p> </p></div> */}
                      </div>
                    </div>
                  </div>
                </div>
                :
                this.state.countrequest_type == 3 ?
                  <div style={{ width: '800px' }} >
                    <div className='navbarrightsss1'>
                      <div className="dro"><img className='dro2' src={sever + '/user_profiles'} onClick={this.onOpenModal} /> </div>
                      <div className="dropbtn1" >
                        <p style={{ fontWeight: 'bold', color: '#05cdff', margin: 'unset', textAlign: 'center' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].role : null}</p>
                        <h4 style={{ color: 'black' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].full_name : null}</h4>
                      </div>
                    </div>


                    <div className='col-md-12'>
                      <div style={{ textAlign: '-webkit-center' }}>
                        <div className='solid'></div>
                      </div>
                      <div><h3 className='h33'>Public Holiday</h3></div>

                      <div className='col-md-6'>

                        <div className='col10' onClick={this._onClick}>
                          <div className='colleft'> <p style={{ fontWeight: 'bold' }}> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_type : null}</p> <p> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_thai : null} </p> </div>
                          <div className='contentright'>
                            <div>
                              <span style={{ marginLeft: 15 }}> {this.state.sumannual}/{this.state.max_day.length > 0 ? this.state.max_day[0].max_day : null} </span>
                            </div>
                            <label style={{ marginRight: 6.5 }}>Days</label>
                          </div>
                        </div>
                      </div>

                      <div className='col-md-6'>
                        <div className='col10' onClick={this._onClick2}>
                          <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[1].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[1].leaName_thai : null}</p></div>
                          <div className='contentright'>
                            <div>
                              <span style={{ marginLeft: 15 }}>{this.state.sumpersonal}/{this.state.max_day.length > 0 ? this.state.max_day[1].max_day : null}</span></div>
                            <label style={{ marginRight: 6.5 }}>Days</label>
                          </div>
                        </div>
                      </div>

                      <div className='col-md-6'>
                        <div className='col10' onClick={this._onClick3}>
                          <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[2].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[2].leaName_thai : null}</p></div>
                          <div className='contentright'>
                            <div>
                              <span style={{ marginLeft: 15 }}>{this.state.sumsick}/{this.state.max_day.length > 0 ? this.state.max_day[2].max_day : null} </span></div>
                            <label style={{ marginRight: 6.5 }}>Days</label>
                          </div>
                        </div>
                      </div>

                      <div className='col-md-6'>
                        <div className='col10' onClick={this._onClick8}>
                          <div className='colleft1'><p style={{ fontWeight: 'bold' }}>Flexible Benefit Request</p> <p> คำขอเกี่ยวกับสวัสดิการอื่นๆ</p></div>
                          {/* <div className='colright'><p> </p></div> */}
                        </div>
                      </div>
                    </div>
                  </div>
                  :
                  this.state.countrequest_type == 4 ?
                    <div style={{ width: '800px' }} >
                      <div className='navbarrightsss1'>
                        <div className="dro"><img className='dro2' src={sever + '/user_profiles'} onClick={this.onOpenModal} /> </div>
                        <div className="dropbtn1" >
                          <p style={{ fontWeight: 'bold', color: '#05cdff', margin: 'unset', textAlign: 'center' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].role : null}</p>
                          <h4 style={{ color: 'black' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].full_name : null}</h4>
                        </div>
                      </div>


                      <div className='col-md-12'>
                        <div style={{ textAlign: '-webkit-center' }}>
                          <div className='solid'></div>
                        </div>
                        <div><h3 className='h33'>Public Holiday</h3></div>

                        <div className='col-md-6'>

                          <div className='col10' onClick={this._onClick}>
                            <div className='colleft'> <p style={{ fontWeight: 'bold' }}> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_type : null}</p> <p> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_thai : null} </p> </div>
                            <div className='contentright'>
                              <div>
                                <span style={{ marginLeft: 15 }}> {this.state.sumannual}/{this.state.max_day.length > 0 ? this.state.max_day[0].max_day : null} </span>
                              </div>
                              <label style={{ marginRight: 6.5 }}>Days</label>
                            </div>
                          </div>
                        </div>

                        <div className='col-md-6'>
                          <div className='col10' onClick={this._onClick2}>
                            <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[1].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[1].leaName_thai : null}</p></div>
                            <div className='contentright'>
                              <div>
                                <span style={{ marginLeft: 15 }}>{this.state.sumpersonal}/{this.state.max_day.length > 0 ? this.state.max_day[1].max_day : null}</span></div>
                              <label style={{ marginRight: 6.5 }}>Days</label>
                            </div>
                          </div>
                        </div>

                        <div className='col-md-6'>
                          <div className='col10' onClick={this._onClick3}>
                            <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[2].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[2].leaName_thai : null}</p></div>
                            <div className='contentright'>
                              <div>
                                <span style={{ marginLeft: 15 }}>{this.state.sumsick}/{this.state.max_day.length > 0 ? this.state.max_day[2].max_day : null} </span></div>
                              <label style={{ marginRight: 6.5 }}>Days</label>
                            </div>
                          </div>
                        </div>

                        <div className='col-md-6'>
                          <div className='col10' onClick={this._onClick4}>
                            <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[3].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[3].leaName_thai : null}</p></div>
                            <div className='contentright'>
                              <div>
                                <span style={{ marginLeft: 15 }}> {this.state.sumenter}/{this.state.max_day.length > 0 ? this.state.max_day[3].max_day : null} </span></div>
                              <label style={{ marginRight: 6.5 }}>Days</label>
                            </div>
                          </div>
                        </div>

                        <div className='col-md-6'>
                          <div className='col10' onClick={this._onClick8}>
                            <div className='colleft1'><p style={{ fontWeight: 'bold' }}>Flexible Benefit Request</p> <p> คำขอเกี่ยวกับสวัสดิการอื่นๆ</p></div>
                            {/* <div className='colright'><p> </p></div> */}
                          </div>
                        </div>
                      </div>
                    </div>
                    :
                    this.state.countrequest_type == 5 ?
                      <div style={{ width: '800px' }} >
                        <div className='navbarrightsss1'>
                          <div className="dro"><img className='dro2' src={sever + '/user_profiles'} onClick={this.onOpenModal} /> </div>
                          <div className="dropbtn1" >
                            <p style={{ fontWeight: 'bold', color: '#05cdff', margin: 'unset', textAlign: 'center' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].role : null}</p>
                            <h4 style={{ color: 'black' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].full_name : null}</h4>
                          </div>
                        </div>


                        <div className='col-md-12'>
                          <div style={{ textAlign: '-webkit-center' }}>
                            <div className='solid'></div>
                          </div>
                          <div><h3 className='h33'>Public Holiday</h3></div>

                          <div className='col-md-6'>

                            <div className='col10' onClick={this._onClick}>
                              <div className='colleft'> <p style={{ fontWeight: 'bold' }}> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_type : null}</p> <p> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_thai : null} </p> </div>
                              <div className='contentright'>
                                <div>
                                  <span style={{ marginLeft: 15 }}> {this.state.sumannual}/{this.state.max_day.length > 0 ? this.state.max_day[0].max_day : null} </span>
                                </div>
                                <label style={{ marginRight: 6.5 }}>Days</label>
                              </div>
                            </div>
                          </div>

                          <div className='col-md-6'>
                            <div className='col10' onClick={this._onClick2}>
                              <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[1].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[1].leaName_thai : null}</p></div>
                              <div className='contentright'>
                                <div>
                                  <span style={{ marginLeft: 15 }}>{this.state.sumpersonal}/{this.state.max_day.length > 0 ? this.state.max_day[1].max_day : null}</span></div>
                                <label style={{ marginRight: 6.5 }}>Days</label>
                              </div>
                            </div>
                          </div>

                          <div className='col-md-6'>
                            <div className='col10' onClick={this._onClick3}>
                              <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[2].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[2].leaName_thai : null}</p></div>
                              <div className='contentright'>
                                <div>
                                  <span style={{ marginLeft: 15 }}>{this.state.sumsick}/{this.state.max_day.length > 0 ? this.state.max_day[2].max_day : null} </span></div>
                                <label style={{ marginRight: 6.5 }}>Days</label>
                              </div>
                            </div>
                          </div>

                          <div className='col-md-6'>
                            <div className='col10' onClick={this._onClick4}>
                              <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[3].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[3].leaName_thai : null}</p></div>
                              <div className='contentright'>
                                <div>
                                  <span style={{ marginLeft: 15 }}> {this.state.sumenter}/{this.state.max_day.length > 0 ? this.state.max_day[3].max_day : null} </span></div>
                                <label style={{ marginRight: 6.5 }}>Days</label>
                              </div>
                            </div>
                          </div>

                          <div className='col-md-6'>
                            <div className='col10' onClick={this._onClick5}>
                              <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[4].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[4].leaName_thai : null}</p></div>
                              <div className='contentright'>
                                <div>
                                  <span style={{ marginLeft: 15 }}> {this.state.summilitary}/{this.state.max_day.length > 0 ? this.state.max_day[4].max_day : null}</span>
                                </div>
                                <label style={{ marginRight: 6.5 }}>Days</label>
                              </div>
                            </div>
                          </div>

                          <div className='col-md-6'>
                            <div className='col10' onClick={this._onClick8}>
                              <div className='colleft1'><p style={{ fontWeight: 'bold' }}>Flexible Benefit Request</p> <p> คำขอเกี่ยวกับสวัสดิการอื่นๆ</p></div>
                              {/* <div className='colright'><p> </p></div> */}
                            </div>
                          </div>
                        </div>
                      </div>
                      :
                      this.state.countrequest_type == 6 ?
                        <div style={{ width: '800px' }} >
                          <div className='navbarrightsss1'>
                            <div className="dro"><img className='dro2' src={sever + '/user_profiles'} onClick={this.onOpenModal} /> </div>
                            <div className="dropbtn1" >
                              <p style={{ fontWeight: 'bold', color: '#05cdff', margin: 'unset', textAlign: 'center' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].role : null}</p>
                              <h4 style={{ color: 'black' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].full_name : null}</h4>
                            </div>
                          </div>


                          <div className='col-md-12'>
                            <div style={{ textAlign: '-webkit-center' }}>
                              <div className='solid'></div>
                            </div>
                            <div><h3 className='h33'>Public Holiday</h3></div>

                            <div className='col-md-6'>

                              <div className='col10' onClick={this._onClick}>
                                <div className='colleft'> <p style={{ fontWeight: 'bold' }}> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_type : null}</p> <p> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_thai : null} </p> </div>
                                <div className='contentright'>
                                  <div>
                                    <span style={{ marginLeft: 15 }}> {this.state.sumannual}/{this.state.max_day.length > 0 ? this.state.max_day[0].max_day : null} </span>
                                  </div>
                                  <label style={{ marginRight: 6.5 }}>Days</label>
                                </div>
                              </div>
                            </div>

                            <div className='col-md-6'>
                              <div className='col10' onClick={this._onClick2}>
                                <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[1].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[1].leaName_thai : null}</p></div>
                                <div className='contentright'>
                                  <div>
                                    <span style={{ marginLeft: 15 }}>{this.state.sumpersonal}/{this.state.max_day.length > 0 ? this.state.max_day[1].max_day : null}</span></div>
                                  <label style={{ marginRight: 6.5 }}>Days</label>
                                </div>
                              </div>
                            </div>

                            <div className='col-md-6'>
                              <div className='col10' onClick={this._onClick3}>
                                <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[2].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[2].leaName_thai : null}</p></div>
                                <div className='contentright'>
                                  <div>
                                    <span style={{ marginLeft: 15 }}>{this.state.sumsick}/{this.state.max_day.length > 0 ? this.state.max_day[2].max_day : null} </span></div>
                                  <label style={{ marginRight: 6.5 }}>Days</label>
                                </div>
                              </div>
                            </div>

                            <div className='col-md-6'>
                              <div className='col10' onClick={this._onClick4}>
                                <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[3].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[3].leaName_thai : null}</p></div>
                                <div className='contentright'>
                                  <div>
                                    <span style={{ marginLeft: 15 }}> {this.state.sumenter}/{this.state.max_day.length > 0 ? this.state.max_day[3].max_day : null} </span></div>
                                  <label style={{ marginRight: 6.5 }}>Days</label>
                                </div>
                              </div>
                            </div>

                            <div className='col-md-6'>
                              <div className='col10' onClick={this._onClick5}>
                                <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[4].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[4].leaName_thai : null}</p></div>
                                <div className='contentright'>
                                  <div>
                                    <span style={{ marginLeft: 15 }}> {this.state.summilitary}/{this.state.max_day.length > 0 ? this.state.max_day[4].max_day : null}</span>
                                  </div>
                                  <label style={{ marginRight: 6.5 }}>Days</label>
                                </div>
                              </div>
                            </div>

                            <div className='col-md-6'>
                              <div className='col10' onClick={this._onClick6}>
                                <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[5].leaName_type : null}</p> <p> {this.state.max_day.length > 0 ? this.state.max_day[5].leaName_thai : null}</p></div>
                                <div className='contentright'>
                                  <div>
                                    <span style={{ marginLeft: 15 }}> {this.state.summaternity}/{this.state.max_day.length > 0 ? this.state.max_day[5].max_day : null} </span></div>
                                  <label style={{ marginRight: 6.5 }}>Days</label>
                                </div>
                              </div>
                            </div>

                            <div className='col-md-6'>
                              <div className='col10' onClick={this._onClick8}>
                                <div className='colleft1'><p style={{ fontWeight: 'bold' }}>Flexible Benefit Request</p> <p> คำขอเกี่ยวกับสวัสดิการอื่นๆ</p></div>
                                {/* <div className='colright'><p> </p></div> */}
                              </div>
                            </div>
                          </div>
                        </div>
                        :
                        this.state.countrequest_type == 7 ?
                          <div style={{ width: '800px' }} >
                            <div className='navbarrightsss1'>
                              <div className="dro"><img className='dro2' src={sever + '/user_profiles'} onClick={this.onOpenModal} /> </div>
                              <div className="dropbtn1" >
                                <p style={{ fontWeight: 'bold', color: '#05cdff', margin: 'unset', textAlign: 'center' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].role : null}</p>
                                <h4 style={{ color: 'black' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].full_name : null}</h4>
                              </div>
                            </div>


                            <div className='col-md-12'>
                              <div style={{ textAlign: '-webkit-center' }}>
                                <div className='solid'></div>
                              </div>
                              <div><h3 className='h33'>Public Holiday</h3></div>

                              <div className='col-md-6'>

                                <div className='col10' onClick={this._onClick}>
                                  <div className='colleft'> <p style={{ fontWeight: 'bold' }}> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_type : null}</p> <p> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_thai : null} </p> </div>
                                  <div className='contentright'>
                                    <div>
                                      <span style={{ marginLeft: 15 }}> {this.state.sumannual}/{this.state.max_day.length > 0 ? this.state.max_day[0].max_day : null} </span>
                                    </div>
                                    <label style={{ marginRight: 6.5 }}>Days</label>
                                  </div>
                                </div>
                              </div>

                              <div className='col-md-6'>
                                <div className='col10' onClick={this._onClick2}>
                                  <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[1].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[1].leaName_thai : null}</p></div>
                                  <div className='contentright'>
                                    <div>
                                      <span style={{ marginLeft: 15 }}>{this.state.sumpersonal}/{this.state.max_day.length > 0 ? this.state.max_day[1].max_day : null}</span></div>
                                    <label style={{ marginRight: 6.5 }}>Days</label>
                                  </div>
                                </div>
                              </div>

                              <div className='col-md-6'>
                                <div className='col10' onClick={this._onClick3}>
                                  <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[2].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[2].leaName_thai : null}</p></div>
                                  <div className='contentright'>
                                    <div>
                                      <span style={{ marginLeft: 15 }}>{this.state.sumsick}/{this.state.max_day.length > 0 ? this.state.max_day[2].max_day : null} </span></div>
                                    <label style={{ marginRight: 6.5 }}>Days</label>
                                  </div>
                                </div>
                              </div>

                              <div className='col-md-6'>
                                <div className='col10' onClick={this._onClick4}>
                                  <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[3].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[3].leaName_thai : null}</p></div>
                                  <div className='contentright'>
                                    <div>
                                      <span style={{ marginLeft: 15 }}> {this.state.sumenter}/{this.state.max_day.length > 0 ? this.state.max_day[3].max_day : null} </span></div>
                                    <label style={{ marginRight: 6.5 }}>Days</label>
                                  </div>
                                </div>
                              </div>

                              <div className='col-md-6'>
                                <div className='col10' onClick={this._onClick5}>
                                  <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[4].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[4].leaName_thai : null}</p></div>
                                  <div className='contentright'>
                                    <div>
                                      <span style={{ marginLeft: 15 }}> {this.state.summilitary}/{this.state.max_day.length > 0 ? this.state.max_day[4].max_day : null}</span>
                                    </div>
                                    <label style={{ marginRight: 6.5 }}>Days</label>
                                  </div>
                                </div>
                              </div>

                              <div className='col-md-6'>
                                <div className='col10' onClick={this._onClick6}>
                                  <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[5].leaName_type : null}</p> <p> {this.state.max_day.length > 0 ? this.state.max_day[5].leaName_thai : null}</p></div>
                                  <div className='contentright'>
                                    <div>
                                      <span style={{ marginLeft: 15 }}> {this.state.summaternity}/{this.state.max_day.length > 0 ? this.state.max_day[5].max_day : null} </span></div>
                                    <label style={{ marginRight: 6.5 }}>Days</label>
                                  </div>
                                </div>
                              </div>

                              <div className='col-md-6'>
                                <div className='col10' onClick={this._onClick8}>
                                  <div className='colleft1'><p style={{ fontWeight: 'bold' }}>Flexible Benefit Request</p> <p> คำขอเกี่ยวกับสวัสดิการอื่นๆ</p></div>
                                  {/* <div className='colright'><p> </p></div> */}
                                </div>
                              </div>

                              <div className='col-md-6'  >
                                <div className='col10' onClick={this._onClick7}>
                                  <div className='colleft'><p style={{ fontWeight: 'bold' }}>{this.state.max_day.length > 0 ? this.state.max_day[6].leaName_type : null}</p> <p>{this.state.max_day.length > 0 ? this.state.max_day[6].leaName_thai : null}</p></div>
                                  <div className='contentright'>
                                    <div>
                                      <span style={{ marginLeft: 15 }}>{this.state.sumtraining}/{this.state.max_day.length > 0 ? this.state.max_day[6].max_day : null} </span></div>
                                    <label style={{ marginRight: 6.5 }}>Days</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          :
                          null
          }


        </div >
        <footer className="p_footer">ProjectSoft (Thailand).Co.,Ltd. </footer>
      </div>
    );
  }
}
