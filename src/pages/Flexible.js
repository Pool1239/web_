import React, { Component } from 'react';
import './Flexible.css'
import { post } from '../service/api'
import FileBase64 from '../lib/react-file-base64/src/js/components/react-file-base64';
import swal from 'sweetalert'

export default class Flexible extends Component {


  constructor(props) {
    super(props)

    this.state = {
      ben_type: '',
      // ben_date_request: '',
      ben_date_receipt: '',
      amount: '',
      files: [],
      result: {
        userName: ''
      }
    }
  }
  _onChange = e => {
    this.setState({ [e.target.name]: e.target.value })
    // console.log(this.state.userName)
  }

  getFiles(files) {
    this.setState({
      files: files,
      imagePreviewUrl: files[0].base64
    });
    console.log(files[0].base64)
  }

  _onClick2 = async () => {
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    if (userdata) {
      this.setState({ userdata })
      if (userdata[0].role_id == 1) {
        this.props.history.push('/leaverequest')
      } else {
        this.props.history.push('/LeaveRequest')
      }
    } else {
      this.setState({ userdata: [] })
    }
  }

  componentWillMount = () => {
    let res = localStorage.getItem('userdata');
    if (res) {
      console.log(JSON.parse(res)[0])
      this.setState({ result: JSON.parse(res)[0] })
    }
  }
  componentDidMount = async () => {
    if (this.state.result.userName) {
    }
    else {
      this.props.history.push('/')
      window.location.reload()
    }
  }
  render() {

    let { imagePreviewUrl } = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<img className='imgUpload' src={imagePreviewUrl} />);
    } else {
      $imagePreview = (<div className="previewText"></div>);
    }

    return (

      <div style={{ height: '-webkit-fill-available' }} >
        <h4 className='headtext'>Flexible Benefit Request</h4>
        {/* <div  style={{ height: '-webkit-fill-available', backgroundColor: '#f3f3f3', display: 'flex', justifyContent: 'center', flexDirection: 'column', textAlign: '-webkit-center' }}>

          <div>
            <select className='select' name="ben_type" id="Flexible Benefit Request" onChange={this._onChange} >
              <option >Perpose for Flexible Benefit</option>
              <option value="1">Optical Purchasing</option>
              <option value="2">Oral Purchasing</option>
              <option value="3">Sport</option>
              <option value="4">Transportation on vacation</option>
            </select>
          </div>
     
          <div>
            <input type="date" name="ben_date_receipt" className="App-date2" onChange={this._onChange} />
          </div>

          <div className='borderamount'  >
            <label className="textamount" for="fname" onChange={this._onChange}  >Amount</label>
            <input type='number' className="inputamount" id="fname" name="amount" placeholder="0.00" onChange={this._onChange} />
          </div>
          
          <div >
            {$imagePreview} </div>
          < div style={{ textAlign: '-webkit-center'}}>
            <FileBase64 multiple={true} onDone={this.getFiles.bind(this)} className='fileBase64' />
          </div>

          <div>
            <button className="button4" onClick={this._onClick2} >Cancel</button>
            <button className="button4" onClick={this._onClick}>Submit </button>
          </div>
        </div> */}
        <div style={{ height: '-webkit-fill-available', display: 'flex', justifyContent: 'center', flexDirection: 'column', textAlign: '-webkit-center', paddingBottom: 20 }}>
          <h4 style={{ color: '#05cdff' }}>Flexible Benefit Request</h4>
          <select className='select' name="ben_type" id="Flexible Benefit Request" onChange={this._onChange} >
            <option >Perpose for Flexible Benefit</option>
            <option value="1">Optical Purchasing</option>
            <option value="2">Oral Purchasing</option>
            <option value="3">Sport</option>
            <option value="4">Transportation on vacation</option>
          </select>
          <input type="date" name="ben_date_receipt" className="App-date2" onChange={this._onChange} />
          <div className='borderamount'  >
            <label className="textamount" for="fname" onChange={this._onChange}  >Amount</label>
            <input type='number' className="inputamount" id="fname" name="amount" placeholder="0.00" onChange={this._onChange} />
          </div>
          {$imagePreview}
          < div style={{ textAlign: '-webkit-center' }}>
            <FileBase64 multiple={true} onDone={this.getFiles.bind(this)} className='fileBase64' /> </div>
          <div>
            <button className="button4" onClick={this._onClick2} >Cancel</button>
            <button className="button4" onClick={this._onClick}>Submit </button>
          </div>
        </div>
        <footer className="p_footer">ProjectSoft (Thailand).Co.,Ltd. </footer>
      </div>

    );
  }
  _onClick = async () => {

    let userdata = JSON.parse(localStorage.getItem("userdata"))

    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    const { ben_type, ben_date_receipt, amount, files } = this.state

    const empID = parseInt(userdata[0].empID)
    const obj = {
      ben_type, ben_date_receipt, amount, empID, image: files[0].base64
    }
    console.log(obj)


    try {
      await post('/insert_flexible', obj)
      swal("Success!", "Create Success", "success", {
        buttons: false,
        timer: 1200,
      }).then(() => {
        window.location.reload()
      })
    } catch (error) {
      swal("Error!", "Error", "error", {
        buttons: false,
        timer: 1200,
      })
    }
  }
}

