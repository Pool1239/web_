import React, { Component } from 'react'
import '../pages/Leave.css'
import { get, post, sever } from '../service/api'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import moment from 'moment';
import searc from '../img/magnifying-glass.png'
import Modal from "react-responsive-modal"
import './company.css'
import add from '../img/magnifying-glass.png'
import { Link } from 'react-router-dom';

export default class job2 extends Component {
    constructor(props) {
        super(props)

        this.state = {
            dataTable: [],
            dataTable2: [],
            result: {
                userName: ''
            },
            userdata: [],
            userdatas: [],
            total_time: '',
            sum_hour: '',
            summin1: '',
            summin2: '',
            sum3: ''
        }
    }
    componentWillMount = () => {
        let res = localStorage.getItem('userdata');
        if (res) {
            this.setState({ result: JSON.parse(res)[0] })
        }
    }
    componentDidMount = async () => {
        if (this.state.result.userName) {
            const q = await get('/show_leave_request')
            if (q.success) {
                const res = await get('/project')
                if (res.success) {

                    let userdata = JSON.parse(localStorage.getItem("job3"))
                    if (userdata) {
                        this.setState({ userdata })
                    } else {
                        this.setState({ userdata: [] })
                    }

                    const date = moment(userdata.date).format('YYYY-MM-DD')
                    const user_id = userdata.user_id
                    const obj = {
                        date, user_id
                    }
                    const aaa = await post('/show_job_2', obj)
                    this.setState({ userdatas: aaa.result })

                    const ress = await post('/show_job_3', obj)
                    this.setState({ sum_hour: ress.result[0].full_times })

                    const sumtime = await post('/sumtime_checkin', obj)
                    this.setState({ summin1: sumtime.result[0].sum_min2 })

                    const sumtime2 = await post('/sumtime_company', { comID: userdata.company })
                    this.setState({ summin2: sumtime2.result[0].sum_min })

                    const sum = ((this.state.summin1 / this.state.summin2) * 100).toFixed(0)
                    this.setState({ sum3: sum })

                } else {
                    this.props.history.push('/page2')
                    window.location.reload()
                }
            } else {
                this.props.history.push('/')
                window.location.reload()
            }
        }
        else {
            this.props.history.push('/')
            window.location.reload()
        }
    }


    render() {
        return (
            <div>
                <div>

                    <div className='company1'>
                        <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%' }}>
                            <div style={{ display: '-webkit-inline-box' }}><h3 style={{ marginRight: 10 }}>{this.state.userdata.full_name}</h3></div>
                            <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                <div style={{ marginRight: 8, fontWeight: 'bold', fontSize: 20 }}>Today works to {this.state.sum3} %</div>

                            </div>
                        </div>

                    </div>

                    <div style={{ marginBottom: '20px' }}>
                        <BootstrapTable search className='boot' data={this.state.userdatas} hover pagination>
                            <TableHeaderColumn width='80' dataField='project_id' dataSort isKey>{'ID'}</TableHeaderColumn>
                            <TableHeaderColumn dataField='project_name' dataSort>{'Project Name'}</TableHeaderColumn>
                            <TableHeaderColumn dataField='date' dataSort dataFormat={(cell) => <p>{moment(cell).format('DD-MM-YYYY')}</p>}>{'Date'}</TableHeaderColumn>
                            <TableHeaderColumn width='150' dataField='hour' dataSort dataFormat={(cell) => cell.toFixed(2).replace(".", ":") + '/' + this.state.sum_hour + ' ' + 'hr.'}>{'Working Hours'}</TableHeaderColumn>
                            <TableHeaderColumn width='130' dataField='percent' dataSort dataFormat={(cell) => cell + '%'}>{'Percent/Hours'}</TableHeaderColumn>
                        </BootstrapTable>
                    </div>
                </div>
            </div>
        )
    }
}
