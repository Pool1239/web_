import React, { Component } from 'react'
import '../pages/create.css'
import { post, get } from '../service/api';
import swal from 'sweetalert'

export default class user extends Component {

    constructor(props) {
        super(props)

        this.state = {
            userName: '',
            emp_name: '',
            emp_last: '',
            email: '',
            pro_period: '1',
            role: '1',
            result: {
                userName: ''
            }
        }
    }

    _onChange = e => {
        this.setState({ [e.target.name]: e.target.value })

    }
    componentWillMount = () => {
        let res = localStorage.getItem('userdata');
        if (res) {
            this.setState({ result: JSON.parse(res)[0] })
        }
    }
    componentDidMount = async () => {
        if (this.state.result.userName) {

            const q = await get('/show_leave_request')
            if (q.success) {
                const res = await get('/show_leave_request')
                if (res.success) {
                    this.setState({ dataTables: res.result })
                } else {
                    this.props.history.push('/LeaveRequest')
                    window.location.reload()
                }
            } else {
                this.props.history.push('/')
                window.location.reload()
            }
        }
        else {
            this.props.history.push('/')
            window.location.reload()
        }
    }
    _onClick2 = async () => {
        window.location.reload()
    }

    render() {
        return (
            <div>
                <div className='cu1'>
                    <h3 > Create User Account</h3>
                </div>
                <div className="App-User">

                    <input type="text" name="userName" placeholder="UserName" className="App-ub" onChange={this._onChange} />
                    <input type="text" name="emp_name" placeholder="Name" className="App-ub" onChange={this._onChange} />
                    <input type="text" name="emp_last" placeholder="Lastname" className="App-ub" onChange={this._onChange} />
                    <input type="text" name="email" placeholder="Email" className="App-ub" onChange={this._onChange} />



                    <select name="pro_period" placeholder="Probation Period" className="App-ub1" onChange={this._onChange} /*value={this.state.role_ID}*/ >
                        <option value="1">Pass</option>
                        <option value="2">4 Month</option>
                    </select>


                    <select name="role" placeholder="User Role" className="App-ub2" onChange={this._onChange} /*value={this.state.pro_period_ID}*/>
                        <option value="1">Admin</option>
                        <option value="2">Employee</option>
                    </select>

                    <div flexDirection="row" className='cu2'>
                        <input type="button" value="Cancel" className="App-button1" onClick={this._onClick2} />
                        <input type="button" value="Create Account" className="App-button" onClick={this._onClick} />
                    </div>

                </div>
            </div>

        )
    }

    _onClick = async () => {
        if (this.state.userName == "" || this.state.emp_name == "" || this.state.emp_last == "" || this.state.email == "") {
            swal("warning!", "Please fill out this form", "warning", {
                buttons: false,
                timer: 1200,
            })
        }
        else {
            let userdata = JSON.parse(localStorage.getItem("userdata"))
            console.log('nobar', userdata)
            if (userdata) {
                this.setState({ userdata })
            } else {
                this.setState({ userdata: [] })
            }

            const { userName, emp_name, emp_last, email, role, pro_period } = this.state
            const user_create_id = parseInt(userdata[0].empID)
            const company = parseInt(userdata[0].company)
            const obj = {
                userName, emp_name, emp_last, email, user_create_id, role, pro_period, company
            }

            console.log(obj)
            try {
                const res = await post('/insert_user_by_company', obj)
                swal("Success!", "Create User Success", "success", {
                    buttons: false,
                    timer: 1200,
                })
                this.props.history.push('/AllUsers')
            } catch (error) {
                swal("Error!", "Error", "error", {
                    buttons: false,
                    timer: 1200,
                })
            }
        }
    }
}
