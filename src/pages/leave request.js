import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Nav, NavItem, NavDropdown, Navbar, MenuItem, Button } from 'react-bootstrap'
import './ps.css'
import G7 from '../img/Group 7.png'
import { get, post, sever } from '../service/api'
import plus from '../img/plus.png'
import plus2 from '../img/plus2.png'
import Modal from "react-responsive-modal"
import aaa from '../img/rotate.png'
import { Row } from 'antd';
var x = [1];
var data = [];
export default class leave_request extends Component {

  constructor(props) {
    super(props)

    this.state = {
      annual: [], timeannual: [], sumannual: '',
      personal: [], sumpersonal: '', timepersonal: [],
      sick: [], sumsick: '', timesickl: [],
      enter: [], sumenter: '', timeenter: [],
      military: [], summilitary: '', timemilitary: [],
      maternity: [], summaternity: '', timematernity: [],
      training: [], sumtraining: '', timetraining: [],
      userdata: [],
      max_day: [],
      countrequest_type: '',
      result: {
        userName: ''
      },
      leaName_type: '',
      leaName_thai: '',
      max_day: '',
      IDcompany: '',
      showdata: [],
      row: []
    }
  }
  state = {
    open: false,
  };
  onOpenModal = async () => {
    try {
      const res = await post('/show_company2', { comID: this.state.result.company })
      if (res.success) {
        this.setState({ open: true, showdata: res.result })
      } else {
        alert(res.message)
      }
    } catch (error) {
      alert(error)
    }
  }

  onCloseModal = () => {
    this.setState({ open: false })
    window.location.reload()
  }

  componentWillMount = () => {
    let res = localStorage.getItem('userdata');
    this.getRow();
    if (res) {
      this.setState({ result: JSON.parse(res)[0] })
    }
  }
  componentDidMount = async () => {
    if (this.state.result.userName) {
      const q = await get('/show_leave_request')
      if (q.success) {
        try {
          const res = await get('/show_leave_request')
          if (res.success) {
            this.setState({ dataTables: res.result })

            let userdata = JSON.parse(localStorage.getItem("userdata"))
            this.setState({ userdata })

            try {
              const res = await post('/show_request_count_company', { empID: userdata[0].empID })
              this.setState({ countrequest_type: res.result[0].countcompany })
            } catch (error) {
              alert(error)
            }

            try {
              const res = await post('/show_max_day', { empID: userdata[0].empID })
              this.setState({ max_day: res.result })
            } catch (error) {
              alert(error)
            }

            if (this.state.countrequest_type == 1) {
              try {
                const res = await post('/show_my_leave_request_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
                this.setState({ annual: res.result })
                const req = await post('/show_my_leave_request_1_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
                this.setState({ timeannual: req.result })
                this.setState({ sumannual: parseFloat(this.state.annual) + parseFloat(this.state.timeannual) })
              } catch (error) {
                alert(error)
              }
            } else if (this.state.countrequest_type == 2) {
              try {
                const res = await post('/show_my_leave_request_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
                this.setState({ annual: res.result })
                const req = await post('/show_my_leave_request_1_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
                this.setState({ timeannual: req.result })
                this.setState({ sumannual: parseFloat(this.state.annual) + parseFloat(this.state.timeannual) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_2', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
                this.setState({ personal: res.result })
                const req = await post('/show_my_leave_request_2_1', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
                this.setState({ timepersonal: req.result })
                this.setState({ sumpersonal: parseFloat(this.state.personal) + parseFloat(this.state.timepersonal) })
              } catch (error) {
                alert(error)
              }
            } else if (this.state.countrequest_type == 3) {
              try {
                const res = await post('/show_my_leave_request_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
                this.setState({ annual: res.result })
                const req = await post('/show_my_leave_request_1_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
                this.setState({ timeannual: req.result })
                this.setState({ sumannual: parseFloat(this.state.annual) + parseFloat(this.state.timeannual) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_2', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
                this.setState({ personal: res.result })
                const req = await post('/show_my_leave_request_2_1', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
                this.setState({ timepersonal: req.result })
                this.setState({ sumpersonal: parseFloat(this.state.personal) + parseFloat(this.state.timepersonal) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_3', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
                this.setState({ sick: res.result })
                const req = await post('/show_my_leave_request_3_1', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
                this.setState({ timesickl: req.result })
                this.setState({ sumsick: parseFloat(this.state.sick) + parseFloat(this.state.timesickl) })
              } catch (error) {
                alert(error)
              }
            } else if (this.state.countrequest_type == 4) {
              try {
                const res = await post('/show_my_leave_request_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
                this.setState({ annual: res.result })
                const req = await post('/show_my_leave_request_1_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
                this.setState({ timeannual: req.result })
                this.setState({ sumannual: parseFloat(this.state.annual) + parseFloat(this.state.timeannual) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_2', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
                this.setState({ personal: res.result })
                const req = await post('/show_my_leave_request_2_1', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
                this.setState({ timepersonal: req.result })
                this.setState({ sumpersonal: parseFloat(this.state.personal) + parseFloat(this.state.timepersonal) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_3', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
                this.setState({ sick: res.result })
                const req = await post('/show_my_leave_request_3_1', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
                this.setState({ timesickl: req.result })
                this.setState({ sumsick: parseFloat(this.state.sick) + parseFloat(this.state.timesickl) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_4', { empID: userdata[0].empID, lea_type: this.state.max_day[3].leaID_type })
                this.setState({ enter: res.result })
                const req = await post('/show_my_leave_request_4_1', { empID: userdata[0].empID, lea_type: this.state.max_day[3].leaID_type })
                this.setState({ timeenter: req.result })
                this.setState({ sumenter: parseFloat(this.state.enter) + parseFloat(this.state.timeenter) })
              } catch (error) {
                alert(error)
              }
            } else if (this.state.countrequest_type == 5) {
              try {
                const res = await post('/show_my_leave_request_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
                this.setState({ annual: res.result })
                const req = await post('/show_my_leave_request_1_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
                this.setState({ timeannual: req.result })
                this.setState({ sumannual: parseFloat(this.state.annual) + parseFloat(this.state.timeannual) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_2', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
                this.setState({ personal: res.result })
                const req = await post('/show_my_leave_request_2_1', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
                this.setState({ timepersonal: req.result })
                this.setState({ sumpersonal: parseFloat(this.state.personal) + parseFloat(this.state.timepersonal) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_3', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
                this.setState({ sick: res.result })
                const req = await post('/show_my_leave_request_3_1', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
                this.setState({ timesickl: req.result })
                this.setState({ sumsick: parseFloat(this.state.sick) + parseFloat(this.state.timesickl) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_4', { empID: userdata[0].empID, lea_type: this.state.max_day[3].leaID_type })
                this.setState({ enter: res.result })
                const req = await post('/show_my_leave_request_4_1', { empID: userdata[0].empID, lea_type: this.state.max_day[3].leaID_type })
                this.setState({ timeenter: req.result })
                this.setState({ sumenter: parseFloat(this.state.enter) + parseFloat(this.state.timeenter) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_5', { empID: userdata[0].empID, lea_type: this.state.max_day[4].leaID_type })
                this.setState({ military: res.result })
                const req = await post('/show_my_leave_request_5_1', { empID: userdata[0].empID, lea_type: this.state.max_day[4].leaID_type })
                this.setState({ timemilitary: req.result })
                this.setState({ summilitary: parseFloat(this.state.military) + parseFloat(this.state.timemilitary) })
              } catch (error) {
                alert(error)
              }
            } else if (this.state.countrequest_type == 6) {
              try {
                const res = await post('/show_my_leave_request_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
                this.setState({ annual: res.result })
                const req = await post('/show_my_leave_request_1_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
                this.setState({ timeannual: req.result })
                this.setState({ sumannual: parseFloat(this.state.annual) + parseFloat(this.state.timeannual) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_2', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
                this.setState({ personal: res.result })
                const req = await post('/show_my_leave_request_2_1', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
                this.setState({ timepersonal: req.result })
                this.setState({ sumpersonal: parseFloat(this.state.personal) + parseFloat(this.state.timepersonal) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_3', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
                this.setState({ sick: res.result })
                const req = await post('/show_my_leave_request_3_1', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
                this.setState({ timesickl: req.result })
                this.setState({ sumsick: parseFloat(this.state.sick) + parseFloat(this.state.timesickl) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_4', { empID: userdata[0].empID, lea_type: this.state.max_day[3].leaID_type })
                this.setState({ enter: res.result })
                const req = await post('/show_my_leave_request_4_1', { empID: userdata[0].empID, lea_type: this.state.max_day[3].leaID_type })
                this.setState({ timeenter: req.result })
                this.setState({ sumenter: parseFloat(this.state.enter) + parseFloat(this.state.timeenter) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_5', { empID: userdata[0].empID, lea_type: this.state.max_day[4].leaID_type })
                this.setState({ military: res.result })
                const req = await post('/show_my_leave_request_5_1', { empID: userdata[0].empID, lea_type: this.state.max_day[4].leaID_type })
                this.setState({ timemilitary: req.result })
                this.setState({ summilitary: parseFloat(this.state.military) + parseFloat(this.state.timemilitary) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_6', { empID: userdata[0].empID, lea_type: this.state.max_day[5].leaID_type })
                this.setState({ maternity: res.result })
                const req = await post('/show_my_leave_request_6_1', { empID: userdata[0].empID, lea_type: this.state.max_day[5].leaID_type })
                this.setState({ timematernity: req.result })
                this.setState({ summaternity: parseFloat(this.state.maternity) + parseFloat(this.state.timematernity) })
              } catch (error) {
                alert(error)
              }
            } else if (this.state.countrequest_type == 7) {
              try {
                const res = await post('/show_my_leave_request_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
                this.setState({ annual: res.result })
                const req = await post('/show_my_leave_request_1_1', { empID: userdata[0].empID, lea_type: this.state.max_day[0].leaID_type })
                this.setState({ timeannual: req.result })
                this.setState({ sumannual: parseFloat(this.state.annual) + parseFloat(this.state.timeannual) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_2', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
                this.setState({ personal: res.result })
                const req = await post('/show_my_leave_request_2_1', { empID: userdata[0].empID, lea_type: this.state.max_day[1].leaID_type })
                this.setState({ timepersonal: req.result })
                this.setState({ sumpersonal: parseFloat(this.state.personal) + parseFloat(this.state.timepersonal) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_3', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
                this.setState({ sick: res.result })
                const req = await post('/show_my_leave_request_3_1', { empID: userdata[0].empID, lea_type: this.state.max_day[2].leaID_type })
                this.setState({ timesickl: req.result })
                this.setState({ sumsick: parseFloat(this.state.sick) + parseFloat(this.state.timesickl) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_4', { empID: userdata[0].empID, lea_type: this.state.max_day[3].leaID_type })
                this.setState({ enter: res.result })
                const req = await post('/show_my_leave_request_4_1', { empID: userdata[0].empID, lea_type: this.state.max_day[3].leaID_type })
                this.setState({ timeenter: req.result })
                this.setState({ sumenter: parseFloat(this.state.enter) + parseFloat(this.state.timeenter) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_5', { empID: userdata[0].empID, lea_type: this.state.max_day[4].leaID_type })
                this.setState({ military: res.result })
                const req = await post('/show_my_leave_request_5_1', { empID: userdata[0].empID, lea_type: this.state.max_day[4].leaID_type })
                this.setState({ timemilitary: req.result })
                this.setState({ summilitary: parseFloat(this.state.military) + parseFloat(this.state.timemilitary) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_6', { empID: userdata[0].empID, lea_type: this.state.max_day[5].leaID_type })
                this.setState({ maternity: res.result })
                const req = await post('/show_my_leave_request_6_1', { empID: userdata[0].empID, lea_type: this.state.max_day[5].leaID_type })
                this.setState({ timematernity: req.result })
                this.setState({ summaternity: parseFloat(this.state.maternity) + parseFloat(this.state.timematernity) })
              } catch (error) {
                alert(error)
              }
              try {
                const res = await post('/show_my_leave_request_7', { empID: userdata[0].empID, lea_type: this.state.max_day[6].leaID_type })
                this.setState({ training: res.result })
                const req = await post('/show_my_leave_request_7_1', { empID: userdata[0].empID, lea_type: this.state.max_day[6].leaID_type })
                this.setState({ timetraining: req.result })
                this.setState({ sumtraining: parseFloat(this.state.training) + parseFloat(this.state.timetraining) })
              } catch (error) {
                alert(error)
              }
            }
          } else {
            this.props.history.push('/page2')
            window.location.reload()
          }
        } catch (error) {
          alert(error)
        }
      } else {
        this.props.history.push('/')
        window.location.reload()
      }
    } else {
      this.props.history.push('/')
      window.location.reload()
    }
  }
  _onClick1 = async () => {
    const an = this.state.max_day[0].leaID_type
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick2 = async () => {
    const an = this.state.max_day[1].leaID_type
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick3 = async () => {
    const an = this.state.max_day[2].leaID_type
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick4 = async () => {
    const an = this.state.max_day[3].leaID_type
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick5 = async () => {
    const an = this.state.max_day[4].leaID_type
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick6 = async () => {
    const an = this.state.max_day[5].leaID_type
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick7 = async () => {
    const an = this.state.max_day[6].leaID_type
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick8 = async () => {
    this.props.history.push('/flexible')
  }
  _onClick9 = async () => {
    this.props.history.push('/update_leave_type')
  }
  _onChange = (e, i) => {
    // data.push({name: })
    data[i] = { ...data[i], [e.target.name]: e.target.value, IDcompany: this.state.result.company }
    console.log(data)
    // this.setState({ [e.target.name]: e.target.value })
  }
  click = async () => {
    window.location.reload()
  }
  click3 = async () => {
    // window.location.reload()
    x.push(1)
    this.getRow();
  }
  click2 = async (i) => {
    const a = data
    let leaName_type = []
    let leaName_thai = []
    let max_day = []
    let IDcompany = []
    for (let i = 0; i < a.length; i++) {
      leaName_type.push(a[i].leaName_type)
      leaName_thai.push(a[i].leaName_thai)
      max_day.push(a[i].max_day)
      IDcompany.push(a[i].IDcompany)
    }
    const obj = {
      leaName_type, leaName_thai, max_day, IDcompany
    }

    try {
      await post('/insert_lea_type', obj)
      alert('Success')
      window.location.reload()
    } catch (error) {
      alert(error)
    }
  }

  getRow() {
    let row = x.map((ele, i) => {
      return this.renderRow(i)
    })

    this.setState({ row })
  }

  renderRow(i) {
    console.log(i)
    return (
      <div className="App-User555">
        <div >
          <text style={{ marginRight: 5 }}>{i + 1}.</text>
          <input type="text" name="leaName_type" placeholder="LeaveName English" className="App-ub" onChange={(e) => this._onChange(e, i)} />--
      <input type="text" name="leaName_thai" placeholder="LeaveName Thai" className="App-ub" onChange={(e) => this._onChange(e, i)} />--
      <input type="number" name="max_day" placeholder="Max Day" className="App-ub555" onChange={(e) => this._onChange(e, i)} />
        </div>
      </div>)
  }

  render() {
    const { open, row } = this.state;
    return (
      <div>

        <Modal open={open} onClose={this.onCloseModal} center className='ModalApprove' >
          <div className='cu1'>
            <div style={{ marginTop: 20, textAlign: 'center' }}>
              <h3 >{this.state.showdata.length > 0 ? this.state.showdata[0].comname : null}</h3></div>
          </div>
          {row}
          <div className='pluss'><img src={plus2} className='plusimg' onClick={this.click3} /></div>
          <div flexDirection="row" className='cu2555'>
            <input type="button" value="Cancel" className="App-button1" onClick={this.click} />
            <input type="button" value="Create Account" className="App-button" onClick={() => this.click2(this.state.showdata[0].comID)} />
          </div>
        </Modal>

        <div style={{ width: '100%', textAlign: 'center' }}>
          <div className='navbarright1' >
            <div className="droo" >
              <img className='dro2' src={sever + '/user_profiles'} /></div>
            <div className="dropbtn1" >
              <p style={{ fontWeight: 'bold', color: '#05cdff', margin: 'unset', textAlign: 'center' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].role : null}</p>
              <p style={{ fontWeight: 'bold', color: 'black', margin: 'unset' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].full_name : null}</p>
            </div>
          </div>
        </div>


        <div /*className='justifycenter'*/>
          {this.state.countrequest_type == 0 ?
            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', marginTop: 20 }}>
              <div style={{ display: 'inline-flex' }}>
                <div>
                  <div>
                    <div>
                      <div style={{ width: '100%', textAlign: 'center' }}>
                        <text className='leavetype' onClick={this.onOpenModal}>+ Create Leave Type </text>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            :
            this.state.countrequest_type == 1 ?
              <div>
                <div className='pluss'><img src={plus2} className='plusimg' onClick={this.onOpenModal} /><img src={aaa} className='plusimg' onClick={this._onClick9} /> </div>
                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                  {/*----------------  */}
                  <div style={{ display: 'inline-flex' }}>
                    <div style={{ marginRight: '20px', marginBottom: '20px' }}>
                      <div className='block' onClick={this._onClick1}>
                        <div className='contentleft'>
                          <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_type : null} </p>
                          <p> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_thai : null} </p>
                        </div>
                        <div className='contentright'>
                          <div>
                            <span style={{ marginLeft: 25 }}> {this.state.sumannual}/{this.state.max_day.length > 0 ? this.state.max_day[0].max_day : null} </span>
                          </div>
                          <label>Days</label>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div className='block' onClick={this._onClick8}>
                        <div style={{ padding: '0 10px', color: 'black' }}>
                          <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Flexible Benefit Request </p>
                          <p> คำขอเกี่ยวกับสวัสดิการอื่นๆ </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              :
              this.state.countrequest_type == 2 ?
                <div>
                  <div className='pluss'><img src={plus2} className='plusimg' onClick={this.onOpenModal} /><img src={aaa} className='plusimg' onClick={this._onClick9} /></div>
                  <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                    {/*----------------  */}
                    <div style={{ display: 'inline-flex' }}>
                      <div style={{ marginRight: '20px', marginBottom: '20px' }}>
                        <div className='block' onClick={this._onClick1}>
                          <div className='contentleft'>
                            <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_type : null} </p>
                            <p> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_thai : null} </p>
                          </div>
                          <div className='contentright'>
                            <div>
                              <span style={{ marginLeft: 25 }}> {this.state.sumannual}/{this.state.max_day.length > 0 ? this.state.max_day[0].max_day : null} </span>
                            </div>
                            <label>Days</label>
                          </div>
                        </div>
                      </div>
                      <div>
                        <div className='block' onClick={this._onClick2}>
                          <div className='contentleft'>
                            <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[1].leaName_type : null} </p>
                            <p>  {this.state.max_day.length > 0 ? this.state.max_day[1].leaName_thai : null} </p>
                          </div>
                          <div className='contentright'>
                            <div>
                              <span style={{ marginLeft: 25 }}>{this.state.sumpersonal}/{this.state.max_day.length > 0 ? this.state.max_day[1].max_day : null} </span></div>
                            <label>Days</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/*----------------  */}
                    <div style={{ display: 'inline-flex' }}>
                      <div>
                        <div>
                          <div className='block22' onClick={this._onClick8}>
                            <div style={{ padding: '0 10px', color: 'black', width: '100%', textAlign: 'center' }}>
                              <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Flexible Benefit Request </p>
                              <p> คำขอเกี่ยวกับสวัสดิการอื่นๆ </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                :
                this.state.countrequest_type == 3 ?
                  <div>
                    <div className='pluss'><img src={plus2} className='plusimg' onClick={this.onOpenModal} /><img src={aaa} className='plusimg' onClick={this._onClick9} /></div>
                    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                      {/*----------------  */}
                      <div style={{ display: 'inline-flex' }}>
                        <div style={{ marginRight: '20px', marginBottom: '20px' }}>
                          <div className='block' onClick={this._onClick1}>
                            <div className='contentleft'>
                              <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_type : null} </p>
                              <p> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_thai : null} </p>
                            </div>
                            <div className='contentright'>
                              <div>
                                <span style={{ marginLeft: 25 }}> {this.state.sumannual}/{this.state.max_day.length > 0 ? this.state.max_day[0].max_day : null} </span>
                              </div>
                              <label>Days</label>
                            </div>
                          </div>
                        </div>
                        <div>
                          <div className='block' onClick={this._onClick2}>
                            <div className='contentleft'>
                              <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[1].leaName_type : null} </p>
                              <p>  {this.state.max_day.length > 0 ? this.state.max_day[1].leaName_thai : null} </p>
                            </div>
                            <div className='contentright'>
                              <div>
                                <span style={{ marginLeft: 25 }}>{this.state.sumpersonal}/{this.state.max_day.length > 0 ? this.state.max_day[1].max_day : null} </span></div>
                              <label>Days</label>
                            </div>
                          </div>
                        </div>
                      </div>
                      {/*----------------  */}
                      <div style={{ display: 'inline-flex' }}>
                        <div style={{ marginRight: '20px', marginBottom: '20px' }} >
                          <div className='block' onClick={this._onClick3}>
                            <div className='contentleft'>
                              <p style={{ fontWeight: 'bold', marginBottom: '0px' }}>{this.state.max_day.length > 0 ? this.state.max_day[2].leaName_type : null}</p>
                              <p> {this.state.max_day.length > 0 ? this.state.max_day[2].leaName_thai : null}</p>
                            </div>
                            <div className='contentright'>
                              <div>
                                <span style={{ marginLeft: 25 }}> {this.state.sumsick}/{this.state.max_day.length > 0 ? this.state.max_day[2].max_day : null} </span></div>
                              <label>Days</label>
                            </div>
                          </div>
                        </div>
                        <div>
                          <div className='block' onClick={this._onClick8}>
                            <div style={{ padding: '0 10px', color: 'black' }}>
                              <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Flexible Benefit Request </p>
                              <p> คำขอเกี่ยวกับสวัสดิการอื่นๆ </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  :
                  this.state.countrequest_type == 4 ?
                    <div>
                      <div className='pluss'><img src={plus2} className='plusimg' onClick={this.onOpenModal} /><img src={aaa} className='plusimg' onClick={this._onClick9} /></div>
                      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                        {/*----------------  */}
                        <div style={{ display: 'inline-flex' }}>
                          <div style={{ marginRight: '20px', marginBottom: '20px' }}>
                            <div className='block' onClick={this._onClick1}>
                              <div className='contentleft'>
                                <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_type : null} </p>
                                <p> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_thai : null} </p>
                              </div>
                              <div className='contentright'>
                                <div>
                                  <span style={{ marginLeft: 25 }}> {this.state.sumannual}/{this.state.max_day.length > 0 ? this.state.max_day[0].max_day : null} </span>
                                </div>
                                <label>Days</label>
                              </div>
                            </div>
                          </div>
                          <div>
                            <div className='block' onClick={this._onClick2}>
                              <div className='contentleft'>
                                <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[1].leaName_type : null} </p>
                                <p>  {this.state.max_day.length > 0 ? this.state.max_day[1].leaName_thai : null} </p>
                              </div>
                              <div className='contentright'>
                                <div>
                                  <span style={{ marginLeft: 25 }}>{this.state.sumpersonal}/{this.state.max_day.length > 0 ? this.state.max_day[1].max_day : null} </span></div>
                                <label>Days</label>
                              </div>
                            </div>
                          </div>
                        </div>
                        {/*----------------  */}
                        <div style={{ display: 'inline-flex' }}>
                          <div style={{ marginRight: '20px', marginBottom: '20px' }} >
                            <div className='block' onClick={this._onClick3}>
                              <div className='contentleft'>
                                <p style={{ fontWeight: 'bold', marginBottom: '0px' }}>{this.state.max_day.length > 0 ? this.state.max_day[2].leaName_type : null}</p>
                                <p> {this.state.max_day.length > 0 ? this.state.max_day[2].leaName_thai : null}</p>
                              </div>
                              <div className='contentright'>
                                <div>
                                  <span style={{ marginLeft: 25 }}> {this.state.sumsick}/{this.state.max_day.length > 0 ? this.state.max_day[2].max_day : null} </span></div>
                                <label>Days</label>
                              </div>
                            </div>
                          </div>
                          <div>
                            <div className='block' onClick={this._onClick4}>
                              <div className='contentleft'>
                                <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[3].leaName_type : null} </p>
                                <p> {this.state.max_day.length > 0 ? this.state.max_day[3].leaName_thai : null} </p>
                              </div>
                              <div className='contentright'>
                                <div>
                                  <span style={{ marginLeft: 25 }}> {this.state.sumenter}/{this.state.max_day.length > 0 ? this.state.max_day[3].max_day : null} </span></div>
                                <label>Days</label>
                              </div>
                            </div>
                          </div>
                        </div>
                        {/*----------------  */}
                        <div style={{ display: 'inline-flex' }}>
                          <div>
                            <div>
                              <div className='block22' onClick={this._onClick8}>
                                <div style={{ padding: '0 10px', color: 'black', width: '100%', textAlign: 'center' }}>
                                  <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Flexible Benefit Request </p>
                                  <p> คำขอเกี่ยวกับสวัสดิการอื่นๆ </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    :
                    this.state.countrequest_type == 5 ?
                      <div>
                        <div className='pluss'><img src={plus2} className='plusimg' onClick={this.onOpenModal} /><img src={aaa} className='plusimg' onClick={this._onClick9} /></div>
                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                          {/*----------------  */}
                          <div style={{ display: 'inline-flex' }}>
                            <div style={{ marginRight: '20px', marginBottom: '20px' }}>
                              <div className='block' onClick={this._onClick1}>
                                <div className='contentleft'>
                                  <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_type : null} </p>
                                  <p> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_thai : null} </p>
                                </div>
                                <div className='contentright'>
                                  <div>
                                    <span style={{ marginLeft: 25 }}> {this.state.sumannual}/{this.state.max_day.length > 0 ? this.state.max_day[0].max_day : null} </span>
                                  </div>
                                  <label>Days</label>
                                </div>
                              </div>
                            </div>
                            <div>
                              <div className='block' onClick={this._onClick2}>
                                <div className='contentleft'>
                                  <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[1].leaName_type : null} </p>
                                  <p>  {this.state.max_day.length > 0 ? this.state.max_day[1].leaName_thai : null} </p>
                                </div>
                                <div className='contentright'>
                                  <div>
                                    <span style={{ marginLeft: 25 }}>{this.state.sumpersonal}/{this.state.max_day.length > 0 ? this.state.max_day[1].max_day : null} </span></div>
                                  <label>Days</label>
                                </div>
                              </div>
                            </div>
                          </div>
                          {/*----------------  */}
                          <div style={{ display: 'inline-flex' }}>
                            <div style={{ marginRight: '20px', marginBottom: '20px' }} >
                              <div className='block' onClick={this._onClick3}>
                                <div className='contentleft'>
                                  <p style={{ fontWeight: 'bold', marginBottom: '0px' }}>{this.state.max_day.length > 0 ? this.state.max_day[2].leaName_type : null}</p>
                                  <p> {this.state.max_day.length > 0 ? this.state.max_day[2].leaName_thai : null}</p>
                                </div>
                                <div className='contentright'>
                                  <div>
                                    <span style={{ marginLeft: 25 }}> {this.state.sumsick}/{this.state.max_day.length > 0 ? this.state.max_day[2].max_day : null} </span></div>
                                  <label>Days</label>
                                </div>
                              </div>
                            </div>
                            <div>
                              <div className='block' onClick={this._onClick4}>
                                <div className='contentleft'>
                                  <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[3].leaName_type : null} </p>
                                  <p> {this.state.max_day.length > 0 ? this.state.max_day[3].leaName_thai : null} </p>
                                </div>
                                <div className='contentright'>
                                  <div>
                                    <span style={{ marginLeft: 25 }}> {this.state.sumenter}/{this.state.max_day.length > 0 ? this.state.max_day[3].max_day : null} </span></div>
                                  <label>Days</label>
                                </div>
                              </div>
                            </div>
                          </div>
                          {/*----------------  */}
                          <div style={{ display: 'inline-flex' }}>
                            <div style={{ marginRight: '20px', marginBottom: '20px' }}>
                              <div className='block' onClick={this._onClick5}>
                                <div className='contentleft'>
                                  <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[4].leaName_type : null} </p>
                                  <p> {this.state.max_day.length > 0 ? this.state.max_day[4].leaName_thai : null} </p>
                                </div>
                                <div className='contentright'>
                                  <div>
                                    <span style={{ marginLeft: 25 }}> {this.state.summilitary}/{this.state.max_day.length > 0 ? this.state.max_day[4].max_day : null} </span>
                                  </div>
                                  <label>Days</label>
                                </div>
                              </div>
                            </div>
                            <div>
                              <div className='block' onClick={this._onClick8}>
                                <div style={{ padding: '0 10px', color: 'black' }}>
                                  <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Flexible Benefit Request </p>
                                  <p> คำขอเกี่ยวกับสวัสดิการอื่นๆ </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      :
                      this.state.countrequest_type == 6 ?
                        <div>
                          <div className='pluss'><img src={plus2} className='plusimg' onClick={this.onOpenModal} /><img src={aaa} className='plusimg' onClick={this._onClick9} /></div>
                          <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            {/*----------------  */}
                            <div style={{ display: 'inline-flex' }}>
                              <div style={{ marginRight: '20px', marginBottom: '20px' }}>
                                <div className='block' onClick={this._onClick1}>
                                  <div className='contentleft'>
                                    <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_type : null} </p>
                                    <p> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_thai : null} </p>
                                  </div>
                                  <div className='contentright'>
                                    <div>
                                      <span style={{ marginLeft: 25 }}> {this.state.sumannual}/{this.state.max_day.length > 0 ? this.state.max_day[0].max_day : null} </span>
                                    </div>
                                    <label>Days</label>
                                  </div>
                                </div>
                              </div>
                              <div>
                                <div className='block' onClick={this._onClick2}>
                                  <div className='contentleft'>
                                    <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[1].leaName_type : null} </p>
                                    <p>  {this.state.max_day.length > 0 ? this.state.max_day[1].leaName_thai : null} </p>
                                  </div>
                                  <div className='contentright'>
                                    <div>
                                      <span style={{ marginLeft: 25 }}>{this.state.sumpersonal}/{this.state.max_day.length > 0 ? this.state.max_day[1].max_day : null} </span></div>
                                    <label>Days</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/*----------------  */}
                            <div style={{ display: 'inline-flex' }}>
                              <div style={{ marginRight: '20px', marginBottom: '20px' }} >
                                <div className='block' onClick={this._onClick3}>
                                  <div className='contentleft'>
                                    <p style={{ fontWeight: 'bold', marginBottom: '0px' }}>{this.state.max_day.length > 0 ? this.state.max_day[2].leaName_type : null}</p>
                                    <p> {this.state.max_day.length > 0 ? this.state.max_day[2].leaName_thai : null}</p>
                                  </div>
                                  <div className='contentright'>
                                    <div>
                                      <span style={{ marginLeft: 25 }}> {this.state.sumsick}/{this.state.max_day.length > 0 ? this.state.max_day[2].max_day : null} </span></div>
                                    <label>Days</label>
                                  </div>
                                </div>
                              </div>
                              <div>
                                <div className='block' onClick={this._onClick4}>
                                  <div className='contentleft'>
                                    <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[3].leaName_type : null} </p>
                                    <p> {this.state.max_day.length > 0 ? this.state.max_day[3].leaName_thai : null} </p>
                                  </div>
                                  <div className='contentright'>
                                    <div>
                                      <span style={{ marginLeft: 25 }}> {this.state.sumenter}/{this.state.max_day.length > 0 ? this.state.max_day[3].max_day : null} </span></div>
                                    <label>Days</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/*----------------  */}
                            <div style={{ display: 'inline-flex' }}>
                              <div style={{ marginRight: '20px', marginBottom: '20px' }}>
                                <div className='block' onClick={this._onClick5}>
                                  <div className='contentleft'>
                                    <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[4].leaName_type : null} </p>
                                    <p> {this.state.max_day.length > 0 ? this.state.max_day[4].leaName_thai : null} </p>
                                  </div>
                                  <div className='contentright'>
                                    <div>
                                      <span style={{ marginLeft: 25 }}> {this.state.summilitary}/{this.state.max_day.length > 0 ? this.state.max_day[4].max_day : null} </span>
                                    </div>
                                    <label>Days</label>
                                  </div>
                                </div>
                              </div>
                              <div>
                                <div className='block' onClick={this._onClick6}>
                                  <div className='contentleft'>
                                    <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[5].leaName_type : null}</p>
                                    <p> {this.state.max_day.length > 0 ? this.state.max_day[5].leaName_thai : null} </p>
                                  </div>
                                  <div className='contentright'>
                                    <div>
                                      <span style={{ marginLeft: 25 }}> {this.state.summaternity}/{this.state.max_day.length > 0 ? this.state.max_day[5].max_day : null} </span></div>
                                    <label>Days</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div style={{ display: 'inline-flex' }}>
                              <div>
                                <div>
                                  <div className='block22' onClick={this._onClick8}>
                                    <div style={{ padding: '0 10px', color: 'black', width: '100%', textAlign: 'center' }}>
                                      <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Flexible Benefit Request </p>
                                      <p> คำขอเกี่ยวกับสวัสดิการอื่นๆ </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        :
                        this.state.countrequest_type == 7 ?
                          <div>
                            <div className='pluss'><img src={aaa} className='plusimg' onClick={this._onClick9} /></div>
                            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                              {/*----------------  */}
                              <div style={{ display: 'inline-flex' }}>
                                <div style={{ marginRight: '20px', marginBottom: '20px' }}>
                                  <div className='block' onClick={this._onClick1}>
                                    <div className='contentleft'>
                                      <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_type : null} </p>
                                      <p> {this.state.max_day.length > 0 ? this.state.max_day[0].leaName_thai : null} </p>
                                    </div>
                                    <div className='contentright'>
                                      <div>
                                        <span style={{ marginLeft: 25 }}> {this.state.sumannual}/{this.state.max_day.length > 0 ? this.state.max_day[0].max_day : null} </span>
                                      </div>
                                      <label>Days</label>
                                    </div>
                                  </div>
                                </div>
                                <div>
                                  <div className='block' onClick={this._onClick2}>
                                    <div className='contentleft'>
                                      <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[1].leaName_type : null} </p>
                                      <p>  {this.state.max_day.length > 0 ? this.state.max_day[1].leaName_thai : null} </p>
                                    </div>
                                    <div className='contentright'>
                                      <div>
                                        <span style={{ marginLeft: 25 }}>{this.state.sumpersonal}/{this.state.max_day.length > 0 ? this.state.max_day[1].max_day : null} </span></div>
                                      <label>Days</label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              {/*----------------  */}
                              <div style={{ display: 'inline-flex' }}>
                                <div style={{ marginRight: '20px', marginBottom: '20px' }} >
                                  <div className='block' onClick={this._onClick3}>
                                    <div className='contentleft'>
                                      <p style={{ fontWeight: 'bold', marginBottom: '0px' }}>{this.state.max_day.length > 0 ? this.state.max_day[2].leaName_type : null}</p>
                                      <p> {this.state.max_day.length > 0 ? this.state.max_day[2].leaName_thai : null}</p>
                                    </div>
                                    <div className='contentright'>
                                      <div>
                                        <span style={{ marginLeft: 25 }}> {this.state.sumsick}/{this.state.max_day.length > 0 ? this.state.max_day[2].max_day : null} </span></div>
                                      <label>Days</label>
                                    </div>
                                  </div>
                                </div>
                                <div>
                                  <div className='block' onClick={this._onClick4}>
                                    <div className='contentleft'>
                                      <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[3].leaName_type : null} </p>
                                      <p> {this.state.max_day.length > 0 ? this.state.max_day[3].leaName_thai : null} </p>
                                    </div>
                                    <div className='contentright'>
                                      <div>
                                        <span style={{ marginLeft: 25 }}> {this.state.sumenter}/{this.state.max_day.length > 0 ? this.state.max_day[3].max_day : null} </span></div>
                                      <label>Days</label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              {/*----------------  */}
                              <div style={{ display: 'inline-flex' }}>
                                <div style={{ marginRight: '20px', marginBottom: '20px' }}>
                                  <div className='block' onClick={this._onClick5}>
                                    <div className='contentleft'>
                                      <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[4].leaName_type : null} </p>
                                      <p> {this.state.max_day.length > 0 ? this.state.max_day[4].leaName_thai : null} </p>
                                    </div>
                                    <div className='contentright'>
                                      <div>
                                        <span style={{ marginLeft: 25 }}> {this.state.summilitary}/{this.state.max_day.length > 0 ? this.state.max_day[4].max_day : null} </span>
                                      </div>
                                      <label>Days</label>
                                    </div>
                                  </div>
                                </div>
                                <div>
                                  <div className='block' onClick={this._onClick6}>
                                    <div className='contentleft'>
                                      <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[5].leaName_type : null}</p>
                                      <p> {this.state.max_day.length > 0 ? this.state.max_day[5].leaName_thai : null} </p>
                                    </div>
                                    <div className='contentright'>
                                      <div>
                                        <span style={{ marginLeft: 25 }}> {this.state.summaternity}/{this.state.max_day.length > 0 ? this.state.max_day[5].max_day : null} </span></div>
                                      <label>Days</label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              {/*----------------  */}
                              <div style={{ display: 'inline-flex' }}>
                                <div style={{ marginRight: '20px', marginBottom: '20px' }}>
                                  <div className='block' onClick={this._onClick7}>
                                    <div className='contentleft'>
                                      <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> {this.state.max_day.length > 0 ? this.state.max_day[6].leaName_type : null} </p>
                                      <p> {this.state.max_day.length > 0 ? this.state.max_day[6].leaName_thai : null} </p>
                                    </div>
                                    <div className='contentright'>
                                      <div>
                                        <span style={{ marginLeft: 25 }}>{this.state.sumtraining}/{this.state.max_day.length > 0 ? this.state.max_day[6].max_day : null}</span></div>
                                      <label>Days</label>
                                    </div>
                                  </div>
                                </div>
                                <div>
                                  <div className='block' onClick={this._onClick8}>
                                    <div style={{ padding: '0 10px', color: 'black' }}>
                                      <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Flexible Benefit Request </p>
                                      <p> คำขอเกี่ยวกับสวัสดิการอื่นๆ </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          :
                          null
          }
        </div>
      </div>
    )
  }
}
