import React, { Component } from 'react';
import '../pages/Leave.css';
import { get, post } from '../service/api';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Modal from "react-responsive-modal";
import './company.css';
import add from '../img/new-user.png';
import swal from 'sweetalert';
import { FormControl, FormGroup, ControlLabel } from 'react-bootstrap';

export default class AllCompany extends Component {

  constructor(props) {
    super(props)

    this.state = {
      comID: '',
      time_in: '',
      time_out: '',
      comnames: '',
      userName: '',
      emp_name: '',
      emp_last: '',
      email: '',
      pro_period: '',
      role: '',
      comname: '',
      dataTables: [],
      showdata: [],
      result: {
        userName: ''
      },
      companyName: '',
      change_name: '',
    }

  }
  state = {
    createEmployee: false,
    changeCompany: false,
    addCompany: false,
  };

  getValidationAddCompanyNameState() {
    const name = this.state.companyName;
    const length = this.state.companyName.length;
    if (length >= 1 && name != ' ') return 'success';
    else if (length >= 1 && name === ' ') return 'warning';
    else if (length === 0) return 'error';
    return null;
  }

  getValidationCompanyNameState() {
    const name = this.state.change_name;
    const length = this.state.change_name.length;
    if (length >= 1 && name != ' ') return 'success';
    else if (length >= 1 && name === ' ') return 'warning';
    else if (length === 0) return 'error';
    return null;
  }

  getValidationCompanyTime_InState() {
    const time_in = this.state.time_in;
    const length = this.state.time_in.length;
    if (length >= 1 && time_in != ' ') return 'success';
    else if (length >= 1 && time_in === ' ') return 'warning';
    else if (length === 0) return 'error';
    return null;
  }

  getValidationCompanyTime_OutState() {
    const time_out = this.state.time_out;
    const length = this.state.time_out.length;
    if (length >= 1 && time_out != ' ') return 'success';
    else if (length >= 1 && time_out === ' ') return 'warning';
    else if (length === 0) return 'error';
    return null;
  }

  getValidationUsernameState() {
    const username = this.state.userName;
    const length = this.state.userName.length;
    if (length >= 1 && username != ' ') return 'success';
    else if (length >= 1 && username === ' ') return 'warning';
    else if (length === 0) return 'error';
    return null;
  }

  getValidationNameState() {
    const name = this.state.emp_name;
    const length = this.state.emp_name.length;
    if (length >= 1 && name != ' ') return 'success';
    else if (length >= 1 && name === ' ') return 'warning';
    else if (length === 0) return 'error';
    return null;
  }

  getValidationLastnameState() {
    const lastname = this.state.emp_last;
    const length = this.state.emp_last.length;
    if (length >= 1 && lastname != ' ') return 'success';
    else if (length >= 1 && lastname === ' ') return 'warning';
    else if (length === 0) return 'error';
    return null;
  }

  getValidationEmailState() {
    const email = this.state.email;
    const length = this.state.email.length;
    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (length >= 1 && email.match(mailformat)) return 'success';
    else if (length >= 1) return 'warning';
    else if (length === 0) return 'error';
    return null;
  }

  getValidationDropdownResultState() {
    const pro = this.state.pro_period;
    if (pro != '') return 'success';
    else if (pro === '') return 'error';
    return null;
  }

  getValidationDropdownRoleState() {
    const role = this.state.role;
    if (role != '') return 'success';
    else if (role === '') return 'error';
    return null;
  }

  handleAddCompany = (e) => {
    this.setState({ companyName: e.target.value });
  }

  handleAddCompanyTime_In = (e) => {
    this.setState({ time_in: e.target.value });
  }

  handleAddCompanyTime_Out = (e) => {
    this.setState({ time_out: e.target.value });
  }

  onCreateEmployee = async (cell) => {
    try {
      const res = await post('/show_company2', { comID: cell })
      if (res.success) {
        this.setState({ createEmployee: true, showdata: res.result })
      } else {
        swal("Error!", res.message, "error", {
          buttons: false,
          timer: 1200,
        })
      }

    } catch (error) {
      swal("Error!", "Error", "error", {
        buttons: false,
        timer: 1200,
      })
    }
  }
  onChangeCompany = async (cell) => {
    try {
      const res = await post('/show_company2', { comID: cell })
      if (res.success) {
        this.setState({ changeCompany: true, comnames: res.result[0].comname, comID: res.result[0].comID, time_in: res.result[0].time_in, time_out: res.result[0].time_out })
      } else {
        swal("Error!", res.message, "error", {
          buttons: false,
          timer: 1200,
        })
      }

    } catch (error) {
      swal("Error!", "Error", "error", {
        buttons: false,
        timer: 1200,
      })
    }
  }
  onAddCompany = async () => {
    if (this.state.companyName == "") {
      swal("Warning!", "Please Enter Company Name", "error", {
        buttons: false,
        timer: 1200,
      });
    } else if (this.state.time_in == "") {
      swal("Warning!", "Please Enter Company Open Time", "error", {
        buttons: false,
        timer: 1200,
      });
    } else if (this.state.time_out == "") {
      swal("Warning!", "Please Enter Company Close Time", "error", {
        buttons: false,
        timer: 1200,
      });
    } else {
      const { companyName,time_in,time_out } = this.state
      const obj = {
        comname: companyName,time_in,time_out
      }
      try {
        await post('/insert_company', obj)
        swal("Success!", "Create Company Success", "success", {
          buttons: false,
          timer: 1200,
        });
        window.location.reload()
      } catch (error) {
        swal("Error!", "Error", "error", {
          buttons: false,
          timer: 1200,
        })
      }
    }
  }
  onCloseCreateEmployee = () => {
    this.setState({ createEmployee: false, userName: '', emp_name: '', emp_last: '', email: '', pro_period: '', role: '', })
  }
  onCloseChangeCompany = () => {
    this.setState({ changeCompany: false })
  }
  onCloseAddCompany = () => {
    this.setState({ addCompany: false, companyName: '', time_in: '', time_out: '' })
  }

  _onChangeName = e => {
    this.setState({ change_name: e.target.value })
  }
  _onChangeTime_In = e => {
    this.setState({ time_in: e.target.value })
  }
  _onChangeTime_Out = e => {
    this.setState({ time_out: e.target.value })
  }

  _onChangeUsername = e => {
    const username = e.target.value;
    this.setState({ userName: username.toLowerCase() })
  }

  _onChangeEmp_name = e => {
    const name = e.target.value;
    this.setState({ emp_name: name[0].toUpperCase() + name.slice(1) })
  }

  _onChangeEmp_last = e => {
    const last = e.target.value;
    this.setState({ emp_last: last[0].toUpperCase() + last.slice(1) })
  }

  _onChangeEmp_email = e => {
    const email = e.target.value;
    this.setState({ email: email.toLowerCase() })
  }

  _onChangeEmp_pro = e => {
    this.setState({
      pro_period: this.inputPro.value
    })
  }

  _onChangeEmp_role = () => {
    this.setState({
      role: this.inputRole.value
    })
  }

  componentWillMount = () => {
    let res = localStorage.getItem('userdata');
    if (res) {
      this.setState({ result: JSON.parse(res)[0] })
    }
    this.timer = null;
  }

  componentDidMount = async () => {
    if (this.state.result.userName) {
      if (this.state.result.company == 1) {
        const res = await get('/show_company')
        if (res.success) {
          this.setState({ dataTables: res.result })
          // alert(JSON.stringify(this.state.dataTables))
        } else {
          this.props.history.push('/LeaveRequest')
          window.location.reload()
        }
      } else {
        localStorage.removeItem("userdata", "an", "job", "job3")
        this.props.history.push('/')
        window.location.reload()
      }
    }
    else {
      this.props.history.push('/')
      window.location.reload()
    }
  }

  _onCreateEmployee = async (comID) => {
    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (this.state.userName == "" || this.state.emp_name == "" || this.state.emp_last == "" || this.state.email == "" || this.state.pro_period === '' || this.state.role === '') {
      swal("Warning!", "Please fill out this form", "warning", {
        buttons: false,
        timer: 1200,
      });
    }
    else if (this.state.email.match(mailformat)) {
      let userdata = JSON.parse(localStorage.getItem("userdata"))
      console.log('nobar', userdata)
      if (userdata) {
        this.setState({ userdata })
      } else {
        this.setState({ userdata: [] })
      }

      const { userName, emp_name, emp_last, email, role, pro_period } = this.state
      const user_create_id = parseInt(userdata[0].empID)
      const company = comID
      const obj = {
        userName, emp_name, emp_last, email, user_create_id, role, pro_period, company
      }
      try {
        const res = await post('/insert_user_by_company', obj)
        if (res.success) {
          swal("Success!", "Employee was added", "success", {
            buttons: false,
            timer: 1200,
          }).then(() => {
            window.location.reload()
          })
        }
        else {
          swal("Error!", "Cannot add employee", "error", {
            buttons: false,
            timer: 1200,
          });
        }
      } catch (error) {
        swal("Error!", "Error", "error", {
          buttons: false,
          timer: 1200,
        })
      }
    }
    else {
      swal("Warning!", "Invalid email", "warning", {
        buttons: false,
        timer: 1200,
      });
    }

  }

  ButtonApprove = (cell, Row) => {
    return (
      <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
        <img
          src={add}
          style={{ marginLeft: 5, marginRight: 10, cursor: 'pointer', width: 25, height: 25 }} onClick={() => this.onCreateEmployee(cell)} />
        <input type="button" value="Edit" className="edit" onClick={() => this.onChangeCompany(cell)} />
      </div>
    )
  }
  _onSubmitEditCompany = async (comID) => {
    const { change_name, time_in, time_out } = this.state;
    if (change_name === '' || time_in === '' || time_out === '') {
      swal("Warning!", "Please enter change data", "warning", {
        buttons: false,
        timer: 1200,
      });
    }
    else {
      const comID = this.state.comID
      const obj = {
        comnames: change_name, comID, time_in, time_out
      }

      try {
        const res = await post('/update_company', obj)
        if (res.success) {
          swal("Success!", "Company has changed", "success", {
            buttons: false,
            timer: 1200,
          }).then(() => {
            window.location.reload()
          })
        }
        else {
          swal("Error!", "Company cannot change", "error", {
            buttons: false,
            timer: 1000,
          });
        }
      } catch (error) {
        swal("Error!", "Error", "error", {
          buttons: false,
          timer: 1200,
        })
      }
    }
  }

  render() {
    const { createEmployee, changeCompany, addCompany } = this.state;
    return (
      <div>

        <div className='company1'>
          <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
            <div><h3>Company</h3></div>
          </div>
          <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
            <input type="button" value="Add" className="add" onClick={() => this.setState({ addCompany: true })} />
          </div>
        </div>

        <div style={{ marginBottom: '20px' }}>
          <BootstrapTable className='boot' data={this.state.dataTables} hover pagination>
            <TableHeaderColumn width={80} dataField='comID' dataSort isKey>{'ID'}</TableHeaderColumn>
            <TableHeaderColumn dataField='comname' dataSort>{'Company'}</TableHeaderColumn>
            <TableHeaderColumn dataField='time_in' dataSort>{'Open'}</TableHeaderColumn>
            <TableHeaderColumn dataField='time_out' dataSort>{'Close'}</TableHeaderColumn>
            <TableHeaderColumn dataField='countcompany' dataSort>{'Employee'}</TableHeaderColumn>
            <TableHeaderColumn width={150} dataField='comID' dataSort dataFormat={this.ButtonApprove}>{}</TableHeaderColumn>
          </BootstrapTable>
        </div>
        <Modal open={createEmployee} onClose={this.onCloseCreateEmployee} center className='ModalApprove' >
          <div style={{ alignItems: 'center', justifyContent: 'center' }}>
            <ControlLabel style={{ marginTop: 10, marginBottom: 20, borderBottom: '2px solid #BDBDBD', padding: 10, fontSize: 20, textAlign: 'center' }}>{this.state.showdata.length > 0 ? this.state.showdata[0].comname : null}</ControlLabel>
            <FormGroup
              controlId="formBasicText"
              validationState={this.getValidationUsernameState()}
            >
              <FormControl
                type="text"
                name="userName"
                value={this.state.userName}
                placeholder="Username"
                onChange={this._onChangeUsername}
              />
              <FormControl.Feedback />
            </FormGroup>
            <FormGroup
              controlId="formBasicText"
              validationState={this.getValidationNameState()}
            >
              <FormControl
                type="text"
                name="name"
                value={this.state.emp_name}
                placeholder="First name"
                onChange={this._onChangeEmp_name}
              />
              <FormControl.Feedback />
            </FormGroup>
            <FormGroup
              controlId="formBasicText"
              validationState={this.getValidationLastnameState()}
            >
              <FormControl
                type="text"
                name="last"
                value={this.state.emp_last}
                placeholder="Last name"
                onChange={this._onChangeEmp_last}
              />
              <FormControl.Feedback />
            </FormGroup>

            <FormGroup
              controlId="formBasicText"
              validationState={this.getValidationEmailState()}
            >
              <FormControl
                type="email"
                name="email"
                value={this.state.email}
                placeholder="Email"
                onChange={this._onChangeEmp_email}
              />
              <FormControl.Feedback />
            </FormGroup>
            <FormGroup
              controlId="formControlsSelect"
              validationState={this.getValidationDropdownResultState()}
            >
              <FormControl
                name="pro"
                value={this.state.pro}
                inputRef={el => this.inputPro = el}
                onChange={this._onChangeEmp_pro}
                style={{ width: '100%', }}
                componentClass="select"
                placeholder="Select Probation Period"
              >
                <option value="">Probation Period</option>
                <option value="1">Pass</option>
                <option value="2">4 Month</option>
              </FormControl>
              <FormControl.Feedback />
            </FormGroup>
            <FormGroup
              controlId="formControlsSelect"
              validationState={this.getValidationDropdownRoleState()}
            >
              <FormControl
                name="role"
                value={this.state.role}
                inputRef={el => this.inputRole = el}
                onChange={this._onChangeEmp_role}
                style={{ width: '100%', }}
                componentClass="select"
                placeholder="Select User Role"
              >
                <option value="">User Role</option>
                <option value="1">Admin</option>
                <option value="2">Employee</option>
              </FormControl>
              <FormControl.Feedback />
            </FormGroup>
            <div flexDirection="row" className='cu2'>
              <input type="button" value="Create Account" className="App-button" onClick={() => this._onCreateEmployee(this.state.showdata[0].comID)} />
            </div>
          </div>
        </Modal>

        <Modal open={changeCompany} onClose={this.onCloseChangeCompany} center className='ModalApprove' >
          <div style={{ alignItems: 'center', justifyContent: 'center' }}>
            <ControlLabel style={{ marginTop: 10, marginBottom: 20, borderBottom: '2px solid #BDBDBD', padding: 10, fontSize: 20, textAlign: 'center' }}>Edit Company</ControlLabel>
            <FormGroup
              controlId="formBasicText"
              validationState={this.getValidationCompanyNameState()}
            >
              <FormControl
                type="text"
                name="comnames"
                value={this.state.change_name}
                placeholder={this.state.comnames}
                onChange={this._onChangeName}
              />
              <FormControl.Feedback />
            </FormGroup>
            <FormGroup
              controlId="formBasicText"
              validationState={this.getValidationCompanyTime_InState()}
            >
              <FormControl
                type="time"
                name="time_in"
                value={this.state.time_in}
                // placeholder={this.state.time_in}
                onChange={this._onChangeTime_In}
              />
              <FormControl.Feedback />
            </FormGroup>
            <FormGroup
              controlId="formBasicText"
              validationState={this.getValidationCompanyTime_OutState()}
            >
              <FormControl
                type="time"
                name="time_out"
                value={this.state.time_out}
                // placeholder={this.state.time_out}
                onChange={this._onChangeTime_Out}
              />
              <FormControl.Feedback />
            </FormGroup>

            <div flexDirection="row" className='cu2'>
              <input type="button" value="OK" className="App-button" onClick={this._onSubmitEditCompany} />
            </div>
          </div>
        </Modal>

        <Modal open={addCompany} onClose={this.onCloseAddCompany} center className='ModalApprove' >
          <div style={{ alignItems: 'center', justifyContent: 'center' }}>
            <ControlLabel style={{ marginTop: 10, marginBottom: 20, borderBottom: '2px solid #BDBDBD', padding: 10, fontSize: 20, textAlign: 'center' }}>Create Company</ControlLabel>
            <FormGroup
              controlId="formBasicText"
              validationState={this.getValidationAddCompanyNameState()}
            >
              <FormControl
                type="text"
                name="comnames"
                value={this.state.companyName}
                placeholder="Enter Company Name"
                onChange={this.handleAddCompany}
              />
              <FormControl.Feedback />
            </FormGroup>
            <FormGroup
              controlId="formBasicText"
              validationState={this.getValidationCompanyTime_InState()}
            >
              <FormControl
                type="time"
                name="time_in"
                value={this.state.time_in}
                placeholder="Enter Company Name"
                onChange={this.handleAddCompanyTime_In}
              />
              <FormControl.Feedback />
            </FormGroup>
            <FormGroup
              controlId="formBasicText"
              validationState={this.getValidationCompanyTime_OutState()}
            >
              <FormControl
                type="time"
                name="time_out"
                value={this.state.time_out}
                placeholder="Enter Company Name"
                onChange={this.handleAddCompanyTime_Out}
              />
              <FormControl.Feedback />
            </FormGroup>

            <div flexDirection="row" className='cu2'>
              <input type="button" value="OK" className="App-button" onClick={this.onAddCompany} />
            </div>
          </div>
        </Modal>
      </div>
    )
  }
}
