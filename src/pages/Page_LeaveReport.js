import React, { Component } from 'react'
import '../pages/Leave.css'
import { get, post, sever } from '../service/api';
import swal from 'sweetalert'

export default class LeaveReport extends Component {
  constructor(props) {
    super(props)

    this.state = {
      date1: '',
      date2: '',
      result: {
        userName: ''
      }
    }
  }

  componentWillMount = () => {
    let res = localStorage.getItem('userdata');
    if (res) {
      this.setState({ result: JSON.parse(res)[0] })
    }
  }
  componentDidMount = async () => {
    if (this.state.result.userName) {
      const q = await get('/show_leave_request')
      if (q.success) {
        try {
          const res = await get('/show_leave_request')
          if (res.success) {
            this.setState({ dataTables: res.result })
          } else {
            this.props.history.push('/page2')
            window.location.reload()
          }
        } catch (error) {
          alert(error)
        }
      } else {
        this.props.history.push('/')
        window.location.reload()
      }
    }
    else {
      this.props.history.push('/')
      window.location.reload()
    }
  }
  _onChange = e => {
    this.setState({ [e.target.name]: e.target.value })

  }

  onSubmit = async () => {
    if (this.state.date1 == "" || this.state.date2 == "") {
      swal("Warning!", "Please fill out this form", "warning", {
        buttons: false,
        timer: 1200,
      })
    }
    else {
      if (this.state.result.company == 1) {
        const { date1, date2 } = this.state
        var win = window.open(sever + "/export_leave_request/" + date1 + "/" + date2, '_blank');
        win.focus()
        window.location.reload()
      } else {
        let userdata = JSON.parse(localStorage.getItem("userdata"))
        console.log('nobar', userdata)
        if (userdata) {
          this.setState({ userdata })
        } else {
          this.setState({ userdata: [] })
        }
        const company = parseInt(userdata[0].company)
        const { date1, date2 } = this.state
        var win = window.open(sever + "/export_leave_request_by_company/" + date1 + "/" + date2 + "/" + company, '_blank');
        win.focus();
        window.location.reload()
        // alert(JSON.stringify(obj))
      }
    }
  }
  onCancel = async () => {
    window.location.reload()
  }

  render() {
    return (
      <div>
        <div className='lea'>
          <h3>Leave Report</h3>
        </div>
        <div className='leave'>
          <div className="App-font">
            <div >
              <input type="date" name="date1" id="dateofbirth" placeholder="From" className="App-date1" onChange={this._onChange} />
              <input type="date" name="date2" id="dateofbirth" placeholder="To" className="App-dates2" onChange={this._onChange} />
            </div>

            {/* <div>
              <input type="text" name="name" placeholder="Export to Email:" className="App-input" className="App-date"/>
            </div> */}

            <div>
              <input type="button" value="Cancel" className="cancel" onClick={this.onCancel} />
              <input type="button" value="Submit" className="submit" onClick={this.onSubmit} />
            </div>
          </div>
        </div>
      </div>
    )
  }
}
