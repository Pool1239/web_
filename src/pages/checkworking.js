import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Nav, NavItem, NavDropdown, Navbar, MenuItem, Button } from 'react-bootstrap'
import './Page_LeaveRequest.css'
import './ps.css'
import './check.css'
import G7 from '../img/Group 7.png'
import { get, post, sever } from '../service/api'
import Modal from "react-responsive-modal"
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import moment from 'moment';
import searc from '../img/magnifying-glass.png'
import './company.css'
import add from '../img/magnifying-glass.png';
import swit from '../img/switch (2).png';
import swal from 'sweetalert'

export default class checkworking extends Component {
    constructor(props) {
        super(props)

        this.state = {
            project_id: '',
            percent: '',
            dataTables: [],
            showdata: [],
            result: {
                userName: ''
            },
            project_name: '',
            userdatas: [],
            full_time: '',
            full_time_2: '',
            hour: '',
            check_in: [],
            date_in: '',
            date_out: '',
            time_in_break: '',
            time_out_break: '',
        }
    }
    state = {
        open: false,
    };
    onOpenModal = async (cell) => {
        try {
            const res = await post('/show_project3', { project_id: cell })
            if (res.success) {
                this.setState({
                    open: true, project_name: res.result[0].project_name, project_id: res.result[0].project_id
                })
            } else {
                swal("Error!", res.message, "error", {
                    buttons: false,
                    timer: 1200,
                })
            }

        } catch (error) {
            swal("Error!", "Error", "error", {
                buttons: false,
                timer: 1200,
            })
        }
    }
    onCloseModal = () => {
        this.setState({ open: false })
        window.location.reload()
    }
    componentWillMount = () => {
        let res = localStorage.getItem('userdata');
        if (res) {
            this.setState({ result: JSON.parse(res)[0] })
        }
    }
    componentDidMount = async () => {
        if (this.state.result.userName) {
            let userdatas = JSON.parse(localStorage.getItem("userdata"))
            if (userdatas) {
                this.setState({ userdatas })
            } else {
                this.setState({ userdatas: [] })
            }
            const res = await post('/show_project_by_company', { company: userdatas[0].company })
            this.setState({ dataTables: res.result })

            const check_in = await post('/show_checkin_3', { user_id: userdatas[0].empID, id_company: userdatas[0].company })
            // alert(JSON.stringify(check_in))
            if (check_in.result.length > 0) {
                this.setState({
                    check_in: check_in.result[0], date_in: check_in.result[0].date_in, date_out: check_in.result[0].date_out
                    , time_in_break: check_in.result[0].time_in_break, time_out_break: check_in.result[0].time_out_break
                })
            }
            const resss = await post('/show_checkin_by_company2', { user_id: userdatas[0].empID })
            this.setState({ showdata: resss.result })
        }
        else {
            this.props.history.push('/')
            window.location.reload()
        }
    }
    ButtonApprove = (cell, Row) => {
        return (
            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <div style={{ flexDirection: 'row', display: 'flex' }}>
                    <input type="button" value="ADD" className="del101" onClick={() => this.onOpenModal(cell)} />
                </div>
                {/* <img src={add} style={{ marginRight: 10, cursor: 'pointer' }} /> */}
            </div>
        )
    }
    clickk = async (cell) => {
        let userdatas = JSON.parse(localStorage.getItem("userdata"))
        if (userdatas) {
            this.setState({ userdatas })
        } else {
            this.setState({ userdatas: [] })
        }
        const ress = await post('/show_checkin_by_company', { user_id: userdatas[0].empID })
        this.setState({ full_time: ress.result[0].full_time, full_time_2: ress.result[0].full_time_2 })

        const y = cell
        const full1 = parseInt(this.state.full_time)
        const full2 = (this.state.full_time_2 * 100) / 60
        const full = full1 + full2
        const x = y / 100
        const h = parseInt(x * full)
        const z = ((y * full) % 100) / 100

        if (z == 1) {
            const hour = full
            this.setState({ hour: hour.toFixed(2).replace(".", ":") })
        }
        else {
            const min = (z / 100) * 60
            const hour = h + min
            this.setState({ hour: hour.toFixed(2).replace(".", ":") })
        }
    }
    _onChange = e => {
        this.clickk(e.target.value);
        this.setState({ percent: e.target.value })

    }
    clickk2 = async () => {
        let userdatas = JSON.parse(localStorage.getItem("userdata"))
        if (userdatas) {
            this.setState({ userdatas })
        } else {
            this.setState({ userdatas: [] })
        }
        const ress = await post('/show_checkin_by_company', { user_id: userdatas[0].empID })
        this.setState({ full_time: ress.result[0].full_time, full_time_2: ress.result[0].full_time_2 })

        const project_id = parseInt(this.state.project_id)
        const y = this.state.percent
        const full1 = parseInt(this.state.full_time)
        const full2 = (this.state.full_time_2 * 100) / 60
        const full = full1 + full2
        const x = y / 100
        const h = parseInt(x * full)
        const z = ((y * full) % 100) / 100
        if (z == 1) {
            const hour = full
            this.setState({ hour })
            const obj = { hour, project_id, user_id: userdatas[0].empID, percent: y }

            try {
                await post('/insert_job', obj)
                swal("Success!", "Success", "success", {
                    buttons: false,
                    timer: 1200,
                }).then(() => {
                    window.location.reload()
                })
            } catch (error) {
                swal("Error!", "Error", "error", {
                    buttons: false,
                    timer: 1200,
                })
            }
        }
        else {
            const min = (z / 100) * 60
            const hour = h + min
            this.setState({ hour })
            const obj = { hour, project_id, user_id: userdatas[0].empID, percent: y }

            try {
                await post('/insert_job', obj)
                swal("Success!", "Success", "success", {
                    buttons: false,
                    timer: 1200,
                }).then(() => {
                    window.location.reload()
                })
            } catch (error) {
                swal("Error!", "Error", "error", {
                    buttons: false,
                    timer: 1200,
                })
            }
        }
    }

    _onclick = async () => {
        let userdatas = JSON.parse(localStorage.getItem("userdata"))
        if (userdatas) {
            this.setState({ userdatas })
        } else {
            this.setState({ userdatas: [] })
        }
        try {
            await post('/insert_checkin', { user_id: userdatas[0].empID, id_company: userdatas[0].company })
            swal("Success!", "Success", "success", {
                buttons: false,
                timer: 1200,
            }).then(() => {
                window.location.reload()
            })
        } catch (error) {
            swal("Error!", "Error", "error", {
                buttons: false,
                timer: 1200,
            })
        }
    }
    _onclick2 = async () => {
        let userdatas = JSON.parse(localStorage.getItem("userdata"))
        if (userdatas) {
            this.setState({ userdatas })
        } else {
            this.setState({ userdatas: [] })
        }
        try {
            await post('/update_checkin', { user_id: userdatas[0].empID, id_company: userdatas[0].company })
            swal("Success!", "Success", "success", {
                buttons: false,
                timer: 1200,
            }).then(() => {
                window.location.reload()
            })
        } catch (error) {
            swal("Error!", "Error", "error", {
                buttons: false,
                timer: 1200,
            })
        }
    }
    _onclick3 = async () => {
        let userdatas = JSON.parse(localStorage.getItem("userdata"))
        if (userdatas) {
            this.setState({ userdatas })
        } else {
            this.setState({ userdatas: [] })
        }
        try {
            await post('/update_checkin2', { user_id: userdatas[0].empID, id_company: userdatas[0].company })
            swal("Success!", "Success", "success", {
                buttons: false,
                timer: 1200,
            }).then(() => {
                window.location.reload()
            })
        } catch (error) {
            swal("Error!", "Error", "error", {
                buttons: false,
                timer: 1200,
            })
        }
    }
    _onclick4 = async () => {
        let userdatas = JSON.parse(localStorage.getItem("userdata"))
        if (userdatas) {
            this.setState({ userdatas })
        } else {
            this.setState({ userdatas: [] })
        }
        try {
            await post('/update_checkin3', { user_id: userdatas[0].empID, id_company: userdatas[0].company })
            swal("Success!", "Success", "success", {
                buttons: false,
                timer: 1200,
            }).then(() => {
                window.location.reload()
            })
        } catch (error) {
            swal("Error!", "Error", "error", {
                buttons: false,
                timer: 1200,
            })
        }
    }
    logout = async () => {
        localStorage.removeItem('userdata')
        this.props.history.push('/')
        window.location.reload()
    }
    render() {
        const { open, check_in, date_in, time_in_break } = this.state;
        console.log(time_in_break != '' && date_in != '')

        return (
            <div>

                <Modal open={open} onClose={this.onCloseModal} center className='ModalApprove' >
                    <div className='cu1'>
                        <div style={{ marginTop: 20, textAlign: 'center' }}>
                            <h3 >Working Hours </h3>
                            <h3>{this.state.project_name}.</h3>
                        </div>
                    </div>
                    <div className="App-User99">
                        <div style={{ display: 'flex', flexDirection: 'row' }}>
                            <input type='number' name="percent" placeholder='Percent%' onChange={this._onChange} style={{ width: 90, paddingLeft: 5, paddingRight: 5 }} />
                            <div style={{ fontWeight: 'bold', fontSize: 20 }}>%</div>
                            <div style={{ fontWeight: 'bold', fontSize: 20, marginLeft: 10 }}>{this.state.hour} hr.</div>
                        </div>
                        <div flexDirection="row" className='cu2'>
                            <input type="button" value="OK" className="App-button" onClick={this.clickk2} />
                        </div>

                    </div>
                </Modal>

                <div className='p_headder'>
                    <div class="col-md-4"></div>
                    <div class="col-md-4" style={{ textAlign: 'center', fontWeight: 'bold', marginTop: '6px' }}>Leave Request & Remaining </div>
                    <div class="col-md-4" onClick={this.logout} style={{ textAlign: 'right', fontSize: '15px', paddingTop: '10px', height: '100%', cursor: 'pointer' }}>
                        <p className='lo'>Logout</p> </div>
                </div>

                {
                    check_in.length == 0 ?
                        <div className='company100'>
                            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', width: '100%', alignItems: 'center' }}>
                                <div style={{ display: '-webkit-inline-box' }}><h3 style={{ marginRight: 10 }}>{this.state.userdatas.length > 0 ? this.state.userdatas[0].full_name : null}</h3></div>
                                <div>
                                    <input type='button' value='Clock In' style={{ width: 100, borderRadius: 5, border: 2, marginRight: 5, height: 30 }} onClick={this._onclick} />
                                </div>
                            </div>
                        </div>
                        :
                        this.state.time_in_break == null && this.state.time_out_break == null && this.state.date_out == null ?
                            <div className='company100'>
                                <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', width: '100%', alignItems: 'center' }}>
                                    <div style={{ display: '-webkit-inline-box' }}><h3 style={{ marginRight: 10 }}>{this.state.userdatas.length > 0 ? this.state.userdatas[0].full_name : null}</h3></div>
                                    <div>
                                        <input type='button' value='Start Break' style={{ width: 100, borderRadius: 5, border: 2, marginRight: 5, height: 30 }} onClick={this._onclick2} />
                                        <input type='button' value='Clock Out' style={{ width: 100, borderRadius: 5, border: 2, marginRight: 5, height: 30 }} onClick={this._onclick4} />
                                    </div>
                                </div>
                            </div>
                            :
                            this.state.time_out_break == null && this.state.date_out == null ?
                                <div className='company100'>
                                    <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', width: '100%', alignItems: 'center' }}>
                                        <div style={{ display: '-webkit-inline-box' }}><h3 style={{ marginRight: 10 }}>{this.state.userdatas.length > 0 ? this.state.userdatas[0].full_name : null}</h3></div>
                                        <div>
                                            <input type='button' value='End Break' style={{ width: 100, borderRadius: 5, border: 2, marginRight: 5, height: 30 }} onClick={this._onclick3} />
                                        </div>
                                    </div>
                                </div>
                                :
                                this.state.date_out == null ?
                                    <div className='company100'>
                                        <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', width: '100%', alignItems: 'center' }}>
                                            <div style={{ display: '-webkit-inline-box' }}><h3 style={{ marginRight: 10 }}>{this.state.userdatas.length > 0 ? this.state.userdatas[0].full_name : null}</h3></div>
                                            <div>
                                                <input type='button' value='Clock Out' style={{ width: 100, borderRadius: 5, border: 2, marginRight: 5, height: 30 }} onClick={this._onclick4} />
                                            </div>
                                        </div>
                                    </div>
                                    :
                                    <div>
                                        <div className='company100'>
                                            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', width: '100%', alignItems: 'center' }}>
                                                <div style={{ display: '-webkit-inline-box' }}><h3 style={{ marginRight: 10 }}>{this.state.userdatas.length > 0 ? this.state.userdatas[0].full_name : null}</h3></div>
                                                <div style={{ fontSize: 15, display: 'flex', flexDirection: 'row' }} >Today's working
                                                <div style={{ fontWeight: 'bold', fontSize: 15, marginLeft: 5, marginRight: 5 }}>{this.state.showdata.length > 0 ? this.state.showdata[0].full_time : null} </div>
                                                    hr.</div>
                                            </div>
                                        </div>
                                        <div style={{ marginBottom: '20px' }}>
                                            <BootstrapTable className='boot' data={this.state.dataTables} hover search pagination>
                                                <TableHeaderColumn width={40} dataField='project_id' dataSort isKey>{'ID'}</TableHeaderColumn>
                                                <TableHeaderColumn width={150} dataField='project_name' dataSort>{'Project Name'}</TableHeaderColumn>
                                                <TableHeaderColumn width={150} dataField='project_start' dataSort dataFormat={(cell) => <p>{moment(cell).format('DD-MM-YYYY')}</p>}>{'Start'}</TableHeaderColumn>
                                                <TableHeaderColumn width={150} dataField='project_end' dataSort dataFormat={(cell) => <p>{moment(cell).format('DD-MM-YYYY')}</p>}>{'End'}</TableHeaderColumn>
                                                <TableHeaderColumn width={50} dataField='project_id' dataSort dataFormat={this.ButtonApprove}>{}</TableHeaderColumn>
                                            </BootstrapTable>
                                        </div>
                                    </div>

                }
                <footer className="p_footer">ProjectSoft (Thailand).Co.,Ltd. </footer>
            </div>
        )
    }
}
