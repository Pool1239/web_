import React, { Component } from 'react';
import './Leave.css';
import './modal.css';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Button } from 'antd/lib/radio';
import { Row } from 'antd';
import searc from '../img/magnifying-glass.png'
import Modal from "react-responsive-modal"
import { get, sever } from '../service/api'
import { post } from '../service/api'
import moment from 'moment';
import swal from 'sweetalert';

export default class AllFlexibleRequest extends Component {
  constructor(props) {
    super(props)

    this.state = {
      open: false,
      dataTables: [],
      showdata: [],
      showdata_2: [],
      benID: 0,
      result: {
        userName: ''
      }
    }
  }
  onOpenModal = async (cell) => {
    console.log(cell)
    this.setState({
      benID: cell
    })
    try {
      const res = await post('/show_benefit_1', { benID: cell })
      if (res.success) {
        this.setState({ open: true, showdata: res.result })
      } else {
        swal("Error!", res.message, "error", {
          buttons: false,
          timer: 1200,
        })
      }

    } catch (error) {
      swal("Error!", "Error", "error", {
        buttons: false,
        timer: 1200,
      })
    }
  }

  onCloseModal = () => {
    this.setState({ open: false })
  }

  componentWillMount = () => {
    let res = localStorage.getItem('userdata');
    if (res) {
      this.setState({ result: JSON.parse(res)[0] })
    }
  }
  componentDidMount = async () => {
    if (this.state.result.userName) {
      const q = await get('/show_leave_request')
      if (q.success) {
        if (this.state.result.company == 1) {
          const res = await get('/show_benefit')
          if (res.success) {
            this.setState({ dataTables: res.result })
            // console.log(res.result)
          } else {
            this.props.history.push('/LeaveRequest')
            window.location.reload()
          }
        } else {
          const res = await post('/show_benefit_by_company', { company: this.state.result.company })
          if (res.success) {
            this.setState({ dataTables: res.result })
          } else {
            this.props.history.push('/LeaveRequest')
            window.location.reload()
          }
        }
      } else {
        this.props.history.push('/')
        window.location.reload()
      }
    }
    else {
      this.props.history.push('/')
      window.location.reload()
    }
  }

  ButtonApprove = (cell, Row) => {
    return (
      <div style={{ display: 'inline-flex' }}>
        {/* <div>
          <img src={searc} style={{ marginRight: 10, cursor: 'pointer' }} onClick={() => this.onOpenModal(cell)} />
        </div> */}
        <div
          onClick={() => this.onOpenModal(cell)}
        >
          {
            Row.sta_name == 'Wait' ?
              <button className='bbb' >Approve</button>
              :
              Row.sta_name == 'Approve' ? <button className='bbb2' disabled >Approve</button>
                :
                <button disabled className='bbb3' >Reject</button>
          }
        </div>
      </div>
    )
  }
  onUpdate = async (cell, benID) => {
    console.log(cell, benID)
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    console.log(userdata)

    try {
      await post('/update_benefit', { add_empID: userdata[0].empID, benID: cell })
      swal("Success!", "Approve Success", "success", {
        buttons: false,
        timer: 1200,
      }).then(() => {
        window.location.reload()
      })
    } catch (error) {
      swal("Error!", "Error", "error", {
        buttons: false,
        timer: 1200,
      })
    }

  }

  onUpdate_1 = async (benID) => {
    console.log(benID)
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    try {
      await post('/update_benefit_1', { add_empID: userdata[0].empID, benID })
      swal("Success!", "Reject Success", "success", {
        buttons: false,
        timer: 1200,
      }).then(() => {
        window.location.reload()
      })
    } catch (error) {
      swal("Error!", "Error", "error", {
        buttons: false,
        timer: 1200,
      })
    }
  }
  onUpdate_2 = async (benID) => {
    console.log(benID)
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    console.log(userdata)

    try {
      await post('/update_benefit', { add_empID: userdata[0].empID, benID })
      swal("Success!", "Approve Success", "success", {
        buttons: false,
        timer: 1200,
      }).then(() => {
        window.location.reload()
      })
    } catch (error) {
      swal("Error!", "Error", "error", {
        buttons: false,
        timer: 1200,
      })
    }
  }
  _status = (cell) => {
    return (
      <div style={{ color: cell === 'Wait' ? '#05cdff' : cell === 'Approve' ? 'green' : 'red', fontWeight: cell === 'Wait' ? null : 500 }} >
        {cell}
      </div>
    )
  }
  render() {
    const { open } = this.state;
    return (
      <div>
        <h3>
          <h2>
            <div className='lea'>
              <p>Flexible Benafit</p>
            </div>
          </h2>
        </h3>

        <div style={{ marginBottom: '20px' }}>
          <BootstrapTable className='boot' data={this.state.dataTables} hover pagination>
            <TableHeaderColumn width={80} dataField='benID' dataSort isKey>{'ID'}</TableHeaderColumn>
            <TableHeaderColumn dataField='full_name' dataSort>{'Employee Name'}</TableHeaderColumn>
            <TableHeaderColumn dataField='comname' dataSort>{'Company Name'}</TableHeaderColumn>
            <TableHeaderColumn dataField='benName_type' dataSort>{'Benefit Type'}</TableHeaderColumn>
            <TableHeaderColumn dataField='ben_date_request' dataSort dataFormat={(cell) => <p>{moment(cell).format('DD-MM-YYYY')}</p>}>{'Date Request'}</TableHeaderColumn>
            <TableHeaderColumn dataField='ben_date_receipt' dataSort dataFormat={(cell) => <p>{moment(cell).format('DD-MM-YYYY')}</p>}>{'Date Receipt'}</TableHeaderColumn>
            <TableHeaderColumn dataField='amount' dataSort>{'Amount'}</TableHeaderColumn>
            <TableHeaderColumn dataField='sta_name' dataFormat={this._status} dataSort>{'Status'}</TableHeaderColumn>
            <TableHeaderColumn dataField='nameuser' dataSort>{'Approver'}</TableHeaderColumn>
            <TableHeaderColumn dataField='flexible_created' dataSort dataFormat={(cell) => <p>{moment(cell).format('DD-MM-YYYY')}</p>}>{'Created'}</TableHeaderColumn>
            <TableHeaderColumn dataField='benID' dataSort dataFormat={this.ButtonApprove} >{'Action'}</TableHeaderColumn>
          </BootstrapTable>
        </div>

        <Modal open={open} onClose={this.onCloseModal} center className='ModalApprove' >
          <div className='headerModal'>
            <p className='textheadermodal'>Flexible Benefit</p>
          </div>
          <div className='F_solidmodal1' />

          <p className='PerposeModal'>{this.state.showdata.length > 0 ? this.state.showdata[0].benName_type : null}</p>

          <div className='datemodalclass'>

            <p className='txtReceiptmodal'>Date Request</p>
            <p className='dateRequestModal'>{moment(this.state.showdata.length > 0 ? this.state.showdata[0].ben_date_request : null).format('DD-MM-YYYY')}</p>

          </div>
          <div className='datemodalclass'>

            <p className='txtReceiptmodal'>Date Receipt</p>
            <p className='dateRecieptModal'>{moment(this.state.showdata.length > 0 ? this.state.showdata[0].ben_date_receipt : null).format('DD-MM-YYYY')}</p>

          </div>

          <div className='FF_img1' ><img className='F_img' src={sever + '/bill/' + this.state.benID} /></div>

          <div flexDirection="row" className='buttonModal1'>

            <button className='reject' onClick={() => this.onUpdate_1(this.state.showdata[0].benID)}>Reject</button>

            <button className='bb' onClick={() => this.onUpdate_2(this.state.showdata[0].benID)}>Approve</button>

          </div>
        </Modal>

      </div>
    );
  }
}