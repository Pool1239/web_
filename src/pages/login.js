import React, { Component } from 'react'
import './login.css'
import { Nav, NavItem, NavDropdown, Navbar, MenuItem, Button } from 'react-bootstrap'
import app_logo from '../img/app_logo.png';
import Digio from '../img/digio_logo.png';
import { Link } from 'react-router-dom';
import PS from '../img/office one logo.png'
import { post } from '../service/api'
import MediaQuery from 'react-responsive';
import swal from 'sweetalert'

export default class login extends Component {
  constructor(props) {
    super(props)

    this.state = {
      userName: '',
      password: '',
    }
  }

  componentDidMount = () => {
    document.addEventListener('keypress', this.onenter)
  }
  componentWillUnmount = () => {
    document.removeEventListener('keypress', this.onenter)
  }

  onenter = (target) => {
    if (target.charCode == 13) {
      this._onClick()
    }
  }

  _onChange = e => {
    this.setState({ [e.target.name]: e.target.value })

    // console.log(this.state.userName)
  }
  _onClick = async () => {
    if (this.state.userName == "" || this.state.password == "") {
      swal("Warning!", "Please fill out this form", "warning", {
        buttons: false,
        timer: 1200,
      }).then(() => {
        window.location.reload()
      })
    } else {
      const { userName, password } = this.state
      const obj = {
        userName, password
      }

      try {
        let result = await post('/web_login', obj)
        // this.setState({ data : result.result })
        localStorage.setItem("userdata", JSON.stringify(result.result))
        // console.log(result)

        if (result.result[0].role === "Admin") {
          this.props.history.push('/AllLeaveRequest')
        }
        else if (result.result[0].role === "Employee") {
          this.props.history.push('/LeaveRequest')
        }
        else {
          swal("Warning!", result.message, "warning", {
            buttons: false,
            timer: 1200,
          })
        }
      } catch (error) {
        swal("Error!", "Error", "error", {
          buttons: false,
          timer: 1200,
        })
      }
    }

  }
  render() {
    return (

      <div style={{ height: '-webkit-fill-available', backgroundColor: '#f3f3f3', display: 'flex', justifyContent: 'center', flexDirection: 'column', textAlign: '-webkit-center' }}>

        <img src={PS} className="l4" />
        <input type="text" name="userName" placeholder="Username" className="l6" onChange={this._onChange} />
        <input type="password" name="password" placeholder="Password" className="l6" onChange={this._onChange} />

        <Button className='l7' onClick={this._onClick}>Login</Button>
        <Link to='/confirm' className="l8" >Forgot your Password?</Link>
        <footer className="l3">ProjectSoft (Thailand).Co.,Ltd. </footer>


      </div>
    )
  }
}
