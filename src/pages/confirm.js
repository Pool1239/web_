import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { Nav, NavItem, NavDropdown, Navbar, MenuItem, Button } from 'react-bootstrap'
import './confirm.css'
import app_logo from '../img/app_logo.png';
import Digio from '../img/digio_logo.png';
import PS from '../img/PSthLogowithOfficeOne.png'
import { get } from '../service/api'
import { post } from '../service/api';
import swal from 'sweetalert';

export default class confirm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            password: '',
            confirm: '',
        }
    }
    _onChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    _onClick = async () => {
        if (this.state.email == "" || this.state.password == "" || this.state.confirm == "") {
            swal("Warning!", "Please fill out this form", "warning", {
                buttons: false,
                timer: 1200,
            })
        }
        else if (this.state.password != this.state.confirm) {
            swal("Error!", "Wrong Password", "error", {
                buttons: false,
                timer: 1200,
            })
        }
        else {
            const { email, password } = this.state
            const obj = {
                email, password
            }
            try {
                let result = await post('/change_password', obj)
                if (result.success) {
                    swal("Success!", "Password has changed", "success", {
                        buttons: false,
                        timer: 1200,
                    }).then(() => {
                        this.props.history.push('/')
                    })
                } else {
                    swal("Warning!", result.message, "warning", {
                        buttons: false,
                        timer: 1200,
                    })
                }

            } catch (error) {
                swal("Error!", "Error", "error", {
                    buttons: false,
                    timer: 1200,
                })
            }
        }
    }
    render() {
        return (
            <div style={{ height: '-webkit-fill-available', backgroundColor: '#f3f3f3', display: 'flex', justifyContent: 'center', flexDirection: 'column', textAlign: '-webkit-center' }}>


                <img src={PS} className="c3" />

                <input type="text" name="email" placeholder="Email (Office Email)" className="c5" onChange={this._onChange} />
                <input type="password" name="password" placeholder="Password" className="c5" onChange={this._onChange} />
                <input type="password" name="confirm" placeholder="Confirm Password" className="c5" onChange={this._onChange} />
                <Button className='l7' onClick={this._onClick}>Confirm</Button>
                <footer className="c7">ProjectSoft (Thailand).Co.,Ltd. </footer>


            </div>
        )
    }
}
