import React, { Component } from 'react';
import './App.css';
import Nobar from './component/nobar'
import Navbar from './component/navbar'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      dataTables: [], dataTables2: [],
      showdata: [],
      result: {
        userName: ''
      }
    }
  }
  componentWillMount = () => {
    let res = localStorage.getItem('userdata');
    if (res) {
      console.log(JSON.parse(res)[0])
      this.setState({ result: JSON.parse(res)[0] })
    }
  }

  render() {
    const { children } = this.props
    return (
      <div>
        {this.state.result.company == 1 ? <Navbar /> : <Nobar />}
        {
          children
        }
        <footer className="App7">ProjectSoft (Thailand).Co.,Ltd. </footer>
      </div>
    );
  }
}
export default App;